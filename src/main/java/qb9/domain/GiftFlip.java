package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.FlipStatus;

/**
 * A GiftFlip.
 */
@Entity
@Table(name = "gift_flip")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GiftFlip implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "summary")
    private String summary;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private FlipStatus status;

    @Column(name = "turn_ratio", precision=10, scale=2)
    private BigDecimal turnRatio;

    @ManyToOne
    private Goal goal;

    public Long getId() {
        return id;
    }

    public GiftFlip setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return code;
    }

    public GiftFlip setCode(String code) {
        this.code = code;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public GiftFlip setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public GiftFlip setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public GiftFlip setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public FlipStatus getStatus() {
        return status;
    }

    public GiftFlip setStatus(FlipStatus status) {
        this.status = status;
        return this;
    }

    public BigDecimal getTurnRatio() {
        return turnRatio;
    }

    public GiftFlip setTurnRatio(BigDecimal turnRatio) {
        this.turnRatio = turnRatio;
        return this;
    }

    public Goal getGoal() {
        return goal;
    }

    public GiftFlip setGoal(Goal goal) {
        this.goal = goal;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GiftFlip giftFlip = (GiftFlip) o;
        if(giftFlip.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, giftFlip.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GiftFlip{" +
            "id=" + id +
            ", code='" + code + "'" +
            ", summary='" + summary + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            ", turnRatio='" + turnRatio + "'" +
            '}';
    }
}
