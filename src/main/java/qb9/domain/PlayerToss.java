package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.TossStatus;

/**
 * A PlayerToss.
 */
@Entity
@Table(name = "player_toss")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlayerToss implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "sended_at")
    private ZonedDateTime sendedAt;

    @Column(name = "processed_at")
    private ZonedDateTime processedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TossStatus status;

    @Column(name = "result")
    private String result;

    @ManyToOne
    private GiftFlip flip;

    @ManyToOne
    private PlayerAccount player;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getSendedAt() {
        return sendedAt;
    }

    public void setSendedAt(ZonedDateTime sendedAt) {
        this.sendedAt = sendedAt;
    }

    public ZonedDateTime getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(ZonedDateTime processedAt) {
        this.processedAt = processedAt;
    }

    public TossStatus getStatus() {
        return status;
    }

    public void setStatus(TossStatus status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public GiftFlip getFlip() {
        return flip;
    }

    public void setFlip(GiftFlip giftFlip) {
        this.flip = giftFlip;
    }

    public PlayerAccount getPlayer() {
        return player;
    }

    public void setPlayer(PlayerAccount playerAccount) {
        this.player = playerAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlayerToss playerToss = (PlayerToss) o;
        if(playerToss.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, playerToss.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PlayerToss{" +
            "id=" + id +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", sendedAt='" + sendedAt + "'" +
            ", processedAt='" + processedAt + "'" +
            ", status='" + status + "'" +
            ", result='" + result + "'" +
            '}';
    }
}
