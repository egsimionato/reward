package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A FlipReward.
 */
@Entity
@Table(name = "flip_reward")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FlipReward implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "p", precision=10, scale=2)
    private BigDecimal p;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @ManyToOne
    private GiftFlip flip;

    @ManyToOne
    private GiftItem reward;

    public Long getId() {
        return id;
    }

    public FlipReward setId(Long id) {
        this.id = id;
        return this;
    }

    public BigDecimal getP() {
        return p;
    }

    public FlipReward setP(BigDecimal p) {
        this.p = p;
        return this;
    }

    public Integer getQty() {
        return qty;
    }

    public FlipReward setQty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public FlipReward setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public FlipReward setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public GiftFlip getFlip() {
        return flip;
    }

    public FlipReward setFlip(GiftFlip giftFlip) {
        this.flip = giftFlip;
        return this;
    }

    public GiftItem getReward() {
        return reward;
    }

    public FlipReward setReward(GiftItem giftItem) {
        this.reward = giftItem;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FlipReward flipReward = (FlipReward) o;
        if(flipReward.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, flipReward.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FlipReward{" +
            "id=" + id +
            ", p='" + p + "'" +
            ", qty='" + qty + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            '}';
    }
}
