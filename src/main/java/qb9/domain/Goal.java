package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Goal.
 */
@Entity
@Table(name = "goal")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Goal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "min_ratio", precision=10, scale=2)
    private BigDecimal minRatio;

    public Long getId() {
        return id;
    }

    public Goal setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public Goal setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Goal setCode(String code) {
        this.code = code;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Goal setDescription(String description) {
        this.description = description;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Goal setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Goal setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public BigDecimal getMinRatio() {
        return minRatio;
    }

    public Goal setMinRatio(BigDecimal minRatio) {
        this.minRatio = minRatio;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Goal goal = (Goal) o;
        if(goal.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, goal.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Goal{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", description='" + description + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", minRatio='" + minRatio + "'" +
            '}';
    }
}
