package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.RewardStrategyStatus;

/**
 * A RandChoiceReward.
 */
@Entity
@Table(name = "rand_choice_reward")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RandChoiceReward implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "code")
    private String code;

    @Column(name = "summary")
    private String summary;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private RewardStrategyStatus status;

    @ManyToOne
    private Goal goal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public RandChoiceReward setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getCode() {
        return code;
    }

    public RandChoiceReward setCode(String code) {
        this.code = code;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public RandChoiceReward setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public RandChoiceReward setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public RandChoiceReward setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public RewardStrategyStatus getStatus() {
        return status;
    }

    public RandChoiceReward setStatus(RewardStrategyStatus status) {
        this.status = status;
        return this;
    }

    public Goal getGoal() {
        return goal;
    }

    public RandChoiceReward setGoal(Goal goal) {
        this.goal = goal;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RandChoiceReward randChoiceReward = (RandChoiceReward) o;
        if(randChoiceReward.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, randChoiceReward.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RandChoiceReward{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", summary='" + summary + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
