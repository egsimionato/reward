package qb9.domain.enumeration;

/**
 * The TossStatus enumeration.
 */
public enum TossStatus {
    SENDED,PROCESSED,ERROR
}
