package qb9.domain.enumeration;

/**
 * The AchievementStatus enumeration.
 */
public enum AchievementStatus {
    NEW,PROCESSED,REWARDED
}
