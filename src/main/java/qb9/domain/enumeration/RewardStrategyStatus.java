package qb9.domain.enumeration;

/**
 * The RewardStrategyStatus enumeration.
 */
public enum RewardStrategyStatus {
    DRAFT,ACTIVE,DISABLED
}
