package qb9.domain.enumeration;

/**
 * The FlipStatus enumeration.
 */
public enum FlipStatus {
    DRAFT,ACTIVE,DISABLED
}
