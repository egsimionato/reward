package qb9.domain.enumeration;

/**
 * The GiftStatus enumeration.
 */
public enum GiftStatus {
    ACTIVE,DISABLED
}
