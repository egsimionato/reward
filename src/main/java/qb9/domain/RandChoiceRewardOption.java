package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A RandChoiceRewardOption.
 */
@Entity
@Table(name = "rand_choice_reward_option")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RandChoiceRewardOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "p", precision=10, scale=2)
    private BigDecimal p;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @ManyToOne
    private RandChoiceReward reward;

    @ManyToOne
    private GiftItem giftItem;

    public Long getId() {
        return id;
    }

    public RandChoiceRewardOption setId(Long id) {
        this.id = id;
        return this;
    }

    public BigDecimal getP() {
        return p;
    }

    public RandChoiceRewardOption setP(BigDecimal p) {
        this.p = p;
        return this;
    }

    public Integer getQty() {
        return qty;
    }

    public RandChoiceRewardOption setQty(Integer qty) {
        this.qty = qty;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public RandChoiceRewardOption setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public RandChoiceRewardOption setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public RandChoiceReward getReward() {
        return reward;
    }

    public RandChoiceRewardOption setReward(RandChoiceReward randChoiceReward) {
        this.reward = randChoiceReward;
        return this;
    }

    public GiftItem getGiftItem() {
        return giftItem;
    }

    public RandChoiceRewardOption setGiftItem(GiftItem giftItem) {
        this.giftItem = giftItem;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RandChoiceRewardOption randChoiceRewardOption = (RandChoiceRewardOption) o;
        if(randChoiceRewardOption.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, randChoiceRewardOption.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RandChoiceRewardOption{" +
            "id=" + id +
            ", p='" + p + "'" +
            ", qty='" + qty + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            '}';
    }
}
