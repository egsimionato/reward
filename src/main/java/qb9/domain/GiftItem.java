package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.GiftStatus;

/**
 * A GiftItem.
 */
@Entity
@Table(name = "gift_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GiftItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private GiftStatus status;

    @Column(name = "unit_pack")
    private Integer unitPack;

    @Column(name = "image_src")
    private String imageSrc;

    public Long getId() {
        return id;
    }

    public GiftItem setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public GiftItem setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getCode() {
        return code;
    }

    public GiftItem setCode(String code) {
        this.code = code;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public GiftItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public GiftItem setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public GiftItem setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public GiftStatus getStatus() {
        return status;
    }

    public GiftItem setStatus(GiftStatus status) {
        this.status = status;
        return this;
    }

    public Integer getUnitPack() {
        return unitPack;
    }

    public GiftItem setUnitPack(Integer unitPack) {
        this.unitPack = unitPack;
        return this;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public GiftItem setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GiftItem giftItem = (GiftItem) o;
        if(giftItem.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, giftItem.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GiftItem{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", description='" + description + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            ", unitPack='" + unitPack + "'" +
            ", imageSrc='" + imageSrc + "'" +
            '}';
    }
}
