package qb9.service;

import qb9.domain.Achievement;
import qb9.web.rest.dto.AchievementDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Achievement.
 */
public interface AchievementService {

    /**
     * Save a achievement.
     * 
     * @param achievementDTO the entity to save
     * @return the persisted entity
     */
    AchievementDTO save(AchievementDTO achievementDTO);

    /**
     *  Get all the achievements.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Achievement> findAll(Pageable pageable);

    /**
     *  Get the "id" achievement.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    AchievementDTO findOne(Long id);

    /**
     *  Delete the "id" achievement.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
