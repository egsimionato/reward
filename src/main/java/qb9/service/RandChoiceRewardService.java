package qb9.service;

import qb9.service.dto.RandChoiceRewardDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing RandChoiceReward.
 */
public interface RandChoiceRewardService {

    /**
     * Save a randChoiceReward.
     *
     * @param randChoiceRewardDTO the entity to save
     * @return the persisted entity
     */
    RandChoiceRewardDTO save(RandChoiceRewardDTO randChoiceRewardDTO);

    /**
     *  Get all the randChoiceRewards.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RandChoiceRewardDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" randChoiceReward.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RandChoiceRewardDTO findOne(Long id);

    /**
     *  Delete the "id" randChoiceReward.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
