package qb9.service;

import qb9.service.dto.RandChoiceRewardOptionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing RandChoiceRewardOption.
 */
public interface RandChoiceRewardOptionService {

    /**
     * Save a randChoiceRewardOption.
     *
     * @param randChoiceRewardOptionDTO the entity to save
     * @return the persisted entity
     */
    RandChoiceRewardOptionDTO save(RandChoiceRewardOptionDTO randChoiceRewardOptionDTO);

    /**
     *  Get all the randChoiceRewardOptions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RandChoiceRewardOptionDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" randChoiceRewardOption.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RandChoiceRewardOptionDTO findOne(Long id);

    /**
     *  Delete the "id" randChoiceRewardOption.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
