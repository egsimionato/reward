package qb9.service;

import qb9.domain.PlayerToss;
import qb9.web.rest.dto.PlayerTossDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing PlayerToss.
 */
public interface PlayerTossService {

    /**
     * Save a playerToss.
     * 
     * @param playerTossDTO the entity to save
     * @return the persisted entity
     */
    PlayerTossDTO save(PlayerTossDTO playerTossDTO);

    /**
     *  Get all the playerTosses.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PlayerToss> findAll(Pageable pageable);

    /**
     *  Get the "id" playerToss.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    PlayerTossDTO findOne(Long id);

    /**
     *  Delete the "id" playerToss.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
