package qb9.service;

import qb9.domain.FlipReward;
import qb9.web.rest.dto.FlipRewardDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing FlipReward.
 */
public interface FlipRewardService {

    /**
     * Save a flipReward.
     * 
     * @param flipRewardDTO the entity to save
     * @return the persisted entity
     */
    FlipRewardDTO save(FlipRewardDTO flipRewardDTO);

    /**
     *  Get all the flipRewards.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FlipReward> findAll(Pageable pageable);

    /**
     *  Get the "id" flipReward.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    FlipRewardDTO findOne(Long id);

    /**
     *  Delete the "id" flipReward.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
