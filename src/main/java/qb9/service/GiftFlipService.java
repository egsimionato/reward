package qb9.service;

import qb9.domain.GiftFlip;
import qb9.web.rest.dto.GiftFlipDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing GiftFlip.
 */
public interface GiftFlipService {

    /**
     * Save a giftFlip.
     * 
     * @param giftFlipDTO the entity to save
     * @return the persisted entity
     */
    GiftFlipDTO save(GiftFlipDTO giftFlipDTO);

    /**
     *  Get all the giftFlips.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<GiftFlip> findAll(Pageable pageable);

    /**
     *  Get the "id" giftFlip.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    GiftFlipDTO findOne(Long id);

    /**
     *  Delete the "id" giftFlip.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
