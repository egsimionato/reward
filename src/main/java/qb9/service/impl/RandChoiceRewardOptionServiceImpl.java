package qb9.service.impl;

import qb9.service.RandChoiceRewardOptionService;
import qb9.domain.RandChoiceRewardOption;
import qb9.repository.RandChoiceRewardOptionRepository;
import qb9.service.dto.RandChoiceRewardOptionDTO;
import qb9.service.mapper.RandChoiceRewardOptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing RandChoiceRewardOption.
 */
@Service
@Transactional
public class RandChoiceRewardOptionServiceImpl implements RandChoiceRewardOptionService{

    private final Logger log = LoggerFactory.getLogger(RandChoiceRewardOptionServiceImpl.class);
    
    @Inject
    private RandChoiceRewardOptionRepository randChoiceRewardOptionRepository;

    @Inject
    private RandChoiceRewardOptionMapper randChoiceRewardOptionMapper;

    /**
     * Save a randChoiceRewardOption.
     *
     * @param randChoiceRewardOptionDTO the entity to save
     * @return the persisted entity
     */
    public RandChoiceRewardOptionDTO save(RandChoiceRewardOptionDTO randChoiceRewardOptionDTO) {
        log.debug("Request to save RandChoiceRewardOption : {}", randChoiceRewardOptionDTO);
        RandChoiceRewardOption randChoiceRewardOption = randChoiceRewardOptionMapper.randChoiceRewardOptionDTOToRandChoiceRewardOption(randChoiceRewardOptionDTO);
        randChoiceRewardOption = randChoiceRewardOptionRepository.save(randChoiceRewardOption);
        RandChoiceRewardOptionDTO result = randChoiceRewardOptionMapper.randChoiceRewardOptionToRandChoiceRewardOptionDTO(randChoiceRewardOption);
        return result;
    }

    /**
     *  Get all the randChoiceRewardOptions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<RandChoiceRewardOptionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RandChoiceRewardOptions");
        Page<RandChoiceRewardOption> result = randChoiceRewardOptionRepository.findAll(pageable);
        return result.map(randChoiceRewardOption -> randChoiceRewardOptionMapper.randChoiceRewardOptionToRandChoiceRewardOptionDTO(randChoiceRewardOption));
    }

    /**
     *  Get one randChoiceRewardOption by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public RandChoiceRewardOptionDTO findOne(Long id) {
        log.debug("Request to get RandChoiceRewardOption : {}", id);
        RandChoiceRewardOption randChoiceRewardOption = randChoiceRewardOptionRepository.findOne(id);
        RandChoiceRewardOptionDTO randChoiceRewardOptionDTO = randChoiceRewardOptionMapper.randChoiceRewardOptionToRandChoiceRewardOptionDTO(randChoiceRewardOption);
        return randChoiceRewardOptionDTO;
    }

    /**
     *  Delete the  randChoiceRewardOption by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RandChoiceRewardOption : {}", id);
        randChoiceRewardOptionRepository.delete(id);
    }
}
