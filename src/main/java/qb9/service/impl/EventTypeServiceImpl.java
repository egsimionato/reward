package qb9.service.impl;

import qb9.service.EventTypeService;
import qb9.domain.EventType;
import qb9.repository.EventTypeRepository;
import qb9.web.rest.dto.EventTypeDTO;
import qb9.web.rest.mapper.EventTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing EventType.
 */
@Service
@Transactional
public class EventTypeServiceImpl implements EventTypeService{

    private final Logger log = LoggerFactory.getLogger(EventTypeServiceImpl.class);
    
    @Inject
    private EventTypeRepository eventTypeRepository;
    
    @Inject
    private EventTypeMapper eventTypeMapper;
    
    /**
     * Save a eventType.
     * 
     * @param eventTypeDTO the entity to save
     * @return the persisted entity
     */
    public EventTypeDTO save(EventTypeDTO eventTypeDTO) {
        log.debug("Request to save EventType : {}", eventTypeDTO);
        EventType eventType = eventTypeMapper.eventTypeDTOToEventType(eventTypeDTO);
        eventType = eventTypeRepository.save(eventType);
        EventTypeDTO result = eventTypeMapper.eventTypeToEventTypeDTO(eventType);
        return result;
    }

    /**
     *  Get all the eventTypes.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<EventType> findAll(Pageable pageable) {
        log.debug("Request to get all EventTypes");
        Page<EventType> result = eventTypeRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one eventType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public EventTypeDTO findOne(Long id) {
        log.debug("Request to get EventType : {}", id);
        EventType eventType = eventTypeRepository.findOne(id);
        EventTypeDTO eventTypeDTO = eventTypeMapper.eventTypeToEventTypeDTO(eventType);
        return eventTypeDTO;
    }

    /**
     *  Delete the  eventType by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete EventType : {}", id);
        eventTypeRepository.delete(id);
    }
}
