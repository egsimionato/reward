package qb9.service.impl;

import qb9.service.RandChoiceRewardService;
import qb9.domain.RandChoiceReward;
import qb9.repository.RandChoiceRewardRepository;
import qb9.service.dto.RandChoiceRewardDTO;
import qb9.service.mapper.RandChoiceRewardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing RandChoiceReward.
 */
@Service
@Transactional
public class RandChoiceRewardServiceImpl implements RandChoiceRewardService{

    private final Logger log = LoggerFactory.getLogger(RandChoiceRewardServiceImpl.class);
    
    @Inject
    private RandChoiceRewardRepository randChoiceRewardRepository;

    @Inject
    private RandChoiceRewardMapper randChoiceRewardMapper;

    /**
     * Save a randChoiceReward.
     *
     * @param randChoiceRewardDTO the entity to save
     * @return the persisted entity
     */
    public RandChoiceRewardDTO save(RandChoiceRewardDTO randChoiceRewardDTO) {
        log.debug("Request to save RandChoiceReward : {}", randChoiceRewardDTO);
        RandChoiceReward randChoiceReward = randChoiceRewardMapper.randChoiceRewardDTOToRandChoiceReward(randChoiceRewardDTO);
        randChoiceReward = randChoiceRewardRepository.save(randChoiceReward);
        RandChoiceRewardDTO result = randChoiceRewardMapper.randChoiceRewardToRandChoiceRewardDTO(randChoiceReward);
        return result;
    }

    /**
     *  Get all the randChoiceRewards.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<RandChoiceRewardDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RandChoiceRewards");
        Page<RandChoiceReward> result = randChoiceRewardRepository.findAll(pageable);
        return result.map(randChoiceReward -> randChoiceRewardMapper.randChoiceRewardToRandChoiceRewardDTO(randChoiceReward));
    }

    /**
     *  Get one randChoiceReward by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public RandChoiceRewardDTO findOne(Long id) {
        log.debug("Request to get RandChoiceReward : {}", id);
        RandChoiceReward randChoiceReward = randChoiceRewardRepository.findOne(id);
        RandChoiceRewardDTO randChoiceRewardDTO = randChoiceRewardMapper.randChoiceRewardToRandChoiceRewardDTO(randChoiceReward);
        return randChoiceRewardDTO;
    }

    /**
     *  Delete the  randChoiceReward by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RandChoiceReward : {}", id);
        randChoiceRewardRepository.delete(id);
    }
}
