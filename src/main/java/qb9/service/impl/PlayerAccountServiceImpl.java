package qb9.service.impl;

import qb9.service.PlayerAccountService;
import qb9.domain.PlayerAccount;
import qb9.repository.PlayerAccountRepository;
import qb9.web.rest.dto.PlayerAccountDTO;
import qb9.web.rest.mapper.PlayerAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing PlayerAccount.
 */
@Service
@Transactional
public class PlayerAccountServiceImpl implements PlayerAccountService{

    private final Logger log = LoggerFactory.getLogger(PlayerAccountServiceImpl.class);
    
    @Inject
    private PlayerAccountRepository playerAccountRepository;
    
    @Inject
    private PlayerAccountMapper playerAccountMapper;
    
    /**
     * Save a playerAccount.
     * 
     * @param playerAccountDTO the entity to save
     * @return the persisted entity
     */
    public PlayerAccountDTO save(PlayerAccountDTO playerAccountDTO) {
        log.debug("Request to save PlayerAccount : {}", playerAccountDTO);
        PlayerAccount playerAccount = playerAccountMapper.playerAccountDTOToPlayerAccount(playerAccountDTO);
        playerAccount = playerAccountRepository.save(playerAccount);
        PlayerAccountDTO result = playerAccountMapper.playerAccountToPlayerAccountDTO(playerAccount);
        return result;
    }

    /**
     *  Get all the playerAccounts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PlayerAccount> findAll(Pageable pageable) {
        log.debug("Request to get all PlayerAccounts");
        Page<PlayerAccount> result = playerAccountRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one playerAccount by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PlayerAccountDTO findOne(Long id) {
        log.debug("Request to get PlayerAccount : {}", id);
        PlayerAccount playerAccount = playerAccountRepository.findOne(id);
        PlayerAccountDTO playerAccountDTO = playerAccountMapper.playerAccountToPlayerAccountDTO(playerAccount);
        return playerAccountDTO;
    }

    /**
     *  Delete the  playerAccount by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PlayerAccount : {}", id);
        playerAccountRepository.delete(id);
    }
}
