package qb9.service.impl;

import qb9.service.GiftFlipService;
import qb9.domain.GiftFlip;
import qb9.repository.GiftFlipRepository;
import qb9.web.rest.dto.GiftFlipDTO;
import qb9.web.rest.mapper.GiftFlipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing GiftFlip.
 */
@Service
@Transactional
public class GiftFlipServiceImpl implements GiftFlipService{

    private final Logger log = LoggerFactory.getLogger(GiftFlipServiceImpl.class);
    
    @Inject
    private GiftFlipRepository giftFlipRepository;
    
    @Inject
    private GiftFlipMapper giftFlipMapper;
    
    /**
     * Save a giftFlip.
     * 
     * @param giftFlipDTO the entity to save
     * @return the persisted entity
     */
    public GiftFlipDTO save(GiftFlipDTO giftFlipDTO) {
        log.debug("Request to save GiftFlip : {}", giftFlipDTO);
        GiftFlip giftFlip = giftFlipMapper.giftFlipDTOToGiftFlip(giftFlipDTO);
        giftFlip = giftFlipRepository.save(giftFlip);
        GiftFlipDTO result = giftFlipMapper.giftFlipToGiftFlipDTO(giftFlip);
        return result;
    }

    /**
     *  Get all the giftFlips.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<GiftFlip> findAll(Pageable pageable) {
        log.debug("Request to get all GiftFlips");
        Page<GiftFlip> result = giftFlipRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one giftFlip by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public GiftFlipDTO findOne(Long id) {
        log.debug("Request to get GiftFlip : {}", id);
        GiftFlip giftFlip = giftFlipRepository.findOne(id);
        GiftFlipDTO giftFlipDTO = giftFlipMapper.giftFlipToGiftFlipDTO(giftFlip);
        return giftFlipDTO;
    }

    /**
     *  Delete the  giftFlip by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GiftFlip : {}", id);
        giftFlipRepository.delete(id);
    }
}
