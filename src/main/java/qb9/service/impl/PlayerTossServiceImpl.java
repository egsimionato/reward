package qb9.service.impl;

import qb9.service.PlayerTossService;
import qb9.domain.PlayerToss;
import qb9.repository.PlayerTossRepository;
import qb9.web.rest.dto.PlayerTossDTO;
import qb9.web.rest.mapper.PlayerTossMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing PlayerToss.
 */
@Service
@Transactional
public class PlayerTossServiceImpl implements PlayerTossService{

    private final Logger log = LoggerFactory.getLogger(PlayerTossServiceImpl.class);
    
    @Inject
    private PlayerTossRepository playerTossRepository;
    
    @Inject
    private PlayerTossMapper playerTossMapper;
    
    /**
     * Save a playerToss.
     * 
     * @param playerTossDTO the entity to save
     * @return the persisted entity
     */
    public PlayerTossDTO save(PlayerTossDTO playerTossDTO) {
        log.debug("Request to save PlayerToss : {}", playerTossDTO);
        PlayerToss playerToss = playerTossMapper.playerTossDTOToPlayerToss(playerTossDTO);
        playerToss = playerTossRepository.save(playerToss);
        PlayerTossDTO result = playerTossMapper.playerTossToPlayerTossDTO(playerToss);
        return result;
    }

    /**
     *  Get all the playerTosses.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PlayerToss> findAll(Pageable pageable) {
        log.debug("Request to get all PlayerTosses");
        Page<PlayerToss> result = playerTossRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one playerToss by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PlayerTossDTO findOne(Long id) {
        log.debug("Request to get PlayerToss : {}", id);
        PlayerToss playerToss = playerTossRepository.findOne(id);
        PlayerTossDTO playerTossDTO = playerTossMapper.playerTossToPlayerTossDTO(playerToss);
        return playerTossDTO;
    }

    /**
     *  Delete the  playerToss by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PlayerToss : {}", id);
        playerTossRepository.delete(id);
    }
}
