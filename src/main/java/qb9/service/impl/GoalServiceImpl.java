package qb9.service.impl;

import qb9.service.GoalService;
import qb9.domain.Goal;
import qb9.repository.GoalRepository;
import qb9.web.rest.dto.GoalDTO;
import qb9.web.rest.mapper.GoalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Goal.
 */
@Service
@Transactional
public class GoalServiceImpl implements GoalService{

    private final Logger log = LoggerFactory.getLogger(GoalServiceImpl.class);
    
    @Inject
    private GoalRepository goalRepository;
    
    @Inject
    private GoalMapper goalMapper;
    
    /**
     * Save a goal.
     * 
     * @param goalDTO the entity to save
     * @return the persisted entity
     */
    public GoalDTO save(GoalDTO goalDTO) {
        log.debug("Request to save Goal : {}", goalDTO);
        Goal goal = goalMapper.goalDTOToGoal(goalDTO);
        goal = goalRepository.save(goal);
        GoalDTO result = goalMapper.goalToGoalDTO(goal);
        return result;
    }

    /**
     *  Get all the goals.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Goal> findAll(Pageable pageable) {
        log.debug("Request to get all Goals");
        Page<Goal> result = goalRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one goal by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public GoalDTO findOne(Long id) {
        log.debug("Request to get Goal : {}", id);
        Goal goal = goalRepository.findOne(id);
        GoalDTO goalDTO = goalMapper.goalToGoalDTO(goal);
        return goalDTO;
    }

    /**
     *  Delete the  goal by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Goal : {}", id);
        goalRepository.delete(id);
    }
}
