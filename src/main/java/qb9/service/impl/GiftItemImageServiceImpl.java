package qb9.service.impl;

import qb9.service.GiftItemImageService;
import qb9.domain.GiftItemImage;
import qb9.repository.GiftItemImageRepository;
import qb9.web.rest.dto.GiftItemImageDTO;
import qb9.web.rest.mapper.GiftItemImageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing GiftItemImage.
 */
@Service
@Transactional
public class GiftItemImageServiceImpl implements GiftItemImageService{

    private final Logger log = LoggerFactory.getLogger(GiftItemImageServiceImpl.class);
    
    @Inject
    private GiftItemImageRepository giftItemImageRepository;
    
    @Inject
    private GiftItemImageMapper giftItemImageMapper;
    
    /**
     * Save a giftItemImage.
     * 
     * @param giftItemImageDTO the entity to save
     * @return the persisted entity
     */
    public GiftItemImageDTO save(GiftItemImageDTO giftItemImageDTO) {
        log.debug("Request to save GiftItemImage : {}", giftItemImageDTO);
        GiftItemImage giftItemImage = giftItemImageMapper.giftItemImageDTOToGiftItemImage(giftItemImageDTO);
        giftItemImage = giftItemImageRepository.save(giftItemImage);
        GiftItemImageDTO result = giftItemImageMapper.giftItemImageToGiftItemImageDTO(giftItemImage);
        return result;
    }

    /**
     *  Get all the giftItemImages.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<GiftItemImage> findAll(Pageable pageable) {
        log.debug("Request to get all GiftItemImages");
        Page<GiftItemImage> result = giftItemImageRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one giftItemImage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public GiftItemImageDTO findOne(Long id) {
        log.debug("Request to get GiftItemImage : {}", id);
        GiftItemImage giftItemImage = giftItemImageRepository.findOne(id);
        GiftItemImageDTO giftItemImageDTO = giftItemImageMapper.giftItemImageToGiftItemImageDTO(giftItemImage);
        return giftItemImageDTO;
    }

    /**
     *  Delete the  giftItemImage by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GiftItemImage : {}", id);
        giftItemImageRepository.delete(id);
    }
}
