package qb9.service.impl;

import qb9.service.AchievementService;
import qb9.domain.Achievement;
import qb9.repository.AchievementRepository;
import qb9.web.rest.dto.AchievementDTO;
import qb9.web.rest.mapper.AchievementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Achievement.
 */
@Service
@Transactional
public class AchievementServiceImpl implements AchievementService{

    private final Logger log = LoggerFactory.getLogger(AchievementServiceImpl.class);
    
    @Inject
    private AchievementRepository achievementRepository;
    
    @Inject
    private AchievementMapper achievementMapper;
    
    /**
     * Save a achievement.
     * 
     * @param achievementDTO the entity to save
     * @return the persisted entity
     */
    public AchievementDTO save(AchievementDTO achievementDTO) {
        log.debug("Request to save Achievement : {}", achievementDTO);
        Achievement achievement = achievementMapper.achievementDTOToAchievement(achievementDTO);
        achievement = achievementRepository.save(achievement);
        AchievementDTO result = achievementMapper.achievementToAchievementDTO(achievement);
        return result;
    }

    /**
     *  Get all the achievements.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Achievement> findAll(Pageable pageable) {
        log.debug("Request to get all Achievements");
        Page<Achievement> result = achievementRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one achievement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public AchievementDTO findOne(Long id) {
        log.debug("Request to get Achievement : {}", id);
        Achievement achievement = achievementRepository.findOne(id);
        AchievementDTO achievementDTO = achievementMapper.achievementToAchievementDTO(achievement);
        return achievementDTO;
    }

    /**
     *  Delete the  achievement by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Achievement : {}", id);
        achievementRepository.delete(id);
    }
}
