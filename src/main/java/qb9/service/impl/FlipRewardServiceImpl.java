package qb9.service.impl;

import qb9.service.FlipRewardService;
import qb9.domain.FlipReward;
import qb9.repository.FlipRewardRepository;
import qb9.web.rest.dto.FlipRewardDTO;
import qb9.web.rest.mapper.FlipRewardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing FlipReward.
 */
@Service
@Transactional
public class FlipRewardServiceImpl implements FlipRewardService{

    private final Logger log = LoggerFactory.getLogger(FlipRewardServiceImpl.class);
    
    @Inject
    private FlipRewardRepository flipRewardRepository;
    
    @Inject
    private FlipRewardMapper flipRewardMapper;
    
    /**
     * Save a flipReward.
     * 
     * @param flipRewardDTO the entity to save
     * @return the persisted entity
     */
    public FlipRewardDTO save(FlipRewardDTO flipRewardDTO) {
        log.debug("Request to save FlipReward : {}", flipRewardDTO);
        FlipReward flipReward = flipRewardMapper.flipRewardDTOToFlipReward(flipRewardDTO);
        flipReward = flipRewardRepository.save(flipReward);
        FlipRewardDTO result = flipRewardMapper.flipRewardToFlipRewardDTO(flipReward);
        return result;
    }

    /**
     *  Get all the flipRewards.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<FlipReward> findAll(Pageable pageable) {
        log.debug("Request to get all FlipRewards");
        Page<FlipReward> result = flipRewardRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one flipReward by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public FlipRewardDTO findOne(Long id) {
        log.debug("Request to get FlipReward : {}", id);
        FlipReward flipReward = flipRewardRepository.findOne(id);
        FlipRewardDTO flipRewardDTO = flipRewardMapper.flipRewardToFlipRewardDTO(flipReward);
        return flipRewardDTO;
    }

    /**
     *  Delete the  flipReward by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete FlipReward : {}", id);
        flipRewardRepository.delete(id);
    }
}
