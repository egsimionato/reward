package qb9.service.impl;

import qb9.service.GiftItemService;
import qb9.domain.GiftItem;
import qb9.repository.GiftItemRepository;
import qb9.web.rest.dto.GiftItemDTO;
import qb9.web.rest.mapper.GiftItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing GiftItem.
 */
@Service
@Transactional
public class GiftItemServiceImpl implements GiftItemService{

    private final Logger log = LoggerFactory.getLogger(GiftItemServiceImpl.class);
    
    @Inject
    private GiftItemRepository giftItemRepository;
    
    @Inject
    private GiftItemMapper giftItemMapper;
    
    /**
     * Save a giftItem.
     * 
     * @param giftItemDTO the entity to save
     * @return the persisted entity
     */
    public GiftItemDTO save(GiftItemDTO giftItemDTO) {
        log.debug("Request to save GiftItem : {}", giftItemDTO);
        GiftItem giftItem = giftItemMapper.giftItemDTOToGiftItem(giftItemDTO);
        giftItem = giftItemRepository.save(giftItem);
        GiftItemDTO result = giftItemMapper.giftItemToGiftItemDTO(giftItem);
        return result;
    }

    /**
     *  Get all the giftItems.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<GiftItem> findAll(Pageable pageable) {
        log.debug("Request to get all GiftItems");
        Page<GiftItem> result = giftItemRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one giftItem by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public GiftItemDTO findOne(Long id) {
        log.debug("Request to get GiftItem : {}", id);
        GiftItem giftItem = giftItemRepository.findOne(id);
        GiftItemDTO giftItemDTO = giftItemMapper.giftItemToGiftItemDTO(giftItem);
        return giftItemDTO;
    }

    /**
     *  Delete the  giftItem by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GiftItem : {}", id);
        giftItemRepository.delete(id);
    }
}
