package qb9.service;

import qb9.domain.GiftItemImage;
import qb9.web.rest.dto.GiftItemImageDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing GiftItemImage.
 */
public interface GiftItemImageService {

    /**
     * Save a giftItemImage.
     * 
     * @param giftItemImageDTO the entity to save
     * @return the persisted entity
     */
    GiftItemImageDTO save(GiftItemImageDTO giftItemImageDTO);

    /**
     *  Get all the giftItemImages.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<GiftItemImage> findAll(Pageable pageable);

    /**
     *  Get the "id" giftItemImage.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    GiftItemImageDTO findOne(Long id);

    /**
     *  Delete the "id" giftItemImage.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
