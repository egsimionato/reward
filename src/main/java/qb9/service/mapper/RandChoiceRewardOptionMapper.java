package qb9.service.mapper;

import qb9.domain.*;
import qb9.service.dto.RandChoiceRewardOptionDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity RandChoiceRewardOption and its DTO RandChoiceRewardOptionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RandChoiceRewardOptionMapper {

    @Mapping(source = "reward.id", target = "rewardId")
    @Mapping(source = "reward.code", target = "rewardCode")
    @Mapping(source = "giftItem.id", target = "giftItemId")
    @Mapping(source = "giftItem.code", target = "giftItemCode")
    RandChoiceRewardOptionDTO randChoiceRewardOptionToRandChoiceRewardOptionDTO(RandChoiceRewardOption randChoiceRewardOption);

    List<RandChoiceRewardOptionDTO> randChoiceRewardOptionsToRandChoiceRewardOptionDTOs(List<RandChoiceRewardOption> randChoiceRewardOptions);

    @Mapping(source = "rewardId", target = "reward")
    @Mapping(source = "giftItemId", target = "giftItem")
    RandChoiceRewardOption randChoiceRewardOptionDTOToRandChoiceRewardOption(RandChoiceRewardOptionDTO randChoiceRewardOptionDTO);

    List<RandChoiceRewardOption> randChoiceRewardOptionDTOsToRandChoiceRewardOptions(List<RandChoiceRewardOptionDTO> randChoiceRewardOptionDTOs);

    default RandChoiceReward randChoiceRewardFromId(Long id) {
        if (id == null) {
            return null;
        }
        RandChoiceReward randChoiceReward = new RandChoiceReward();
        randChoiceReward.setId(id);
        return randChoiceReward;
    }

    default GiftItem giftItemFromId(Long id) {
        if (id == null) {
            return null;
        }
        GiftItem giftItem = new GiftItem();
        giftItem.setId(id);
        return giftItem;
    }
}
