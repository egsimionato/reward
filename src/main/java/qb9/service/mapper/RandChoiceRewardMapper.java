package qb9.service.mapper;

import qb9.domain.*;
import qb9.service.dto.RandChoiceRewardDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity RandChoiceReward and its DTO RandChoiceRewardDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RandChoiceRewardMapper {

    @Mapping(source = "goal.id", target = "goalId")
    @Mapping(source = "goal.code", target = "goalCode")
    RandChoiceRewardDTO randChoiceRewardToRandChoiceRewardDTO(RandChoiceReward randChoiceReward);

    List<RandChoiceRewardDTO> randChoiceRewardsToRandChoiceRewardDTOs(List<RandChoiceReward> randChoiceRewards);

    @Mapping(source = "goalId", target = "goal")
    RandChoiceReward randChoiceRewardDTOToRandChoiceReward(RandChoiceRewardDTO randChoiceRewardDTO);

    List<RandChoiceReward> randChoiceRewardDTOsToRandChoiceRewards(List<RandChoiceRewardDTO> randChoiceRewardDTOs);

    default Goal goalFromId(Long id) {
        if (id == null) {
            return null;
        }
        Goal goal = new Goal();
        goal.setId(id);
        return goal;
    }
}
