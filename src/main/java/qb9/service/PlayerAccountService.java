package qb9.service;

import qb9.domain.PlayerAccount;
import qb9.web.rest.dto.PlayerAccountDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing PlayerAccount.
 */
public interface PlayerAccountService {

    /**
     * Save a playerAccount.
     * 
     * @param playerAccountDTO the entity to save
     * @return the persisted entity
     */
    PlayerAccountDTO save(PlayerAccountDTO playerAccountDTO);

    /**
     *  Get all the playerAccounts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PlayerAccount> findAll(Pageable pageable);

    /**
     *  Get the "id" playerAccount.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    PlayerAccountDTO findOne(Long id);

    /**
     *  Delete the "id" playerAccount.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
