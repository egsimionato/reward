package qb9.service;

import qb9.domain.EventType;
import qb9.web.rest.dto.EventTypeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing EventType.
 */
public interface EventTypeService {

    /**
     * Save a eventType.
     * 
     * @param eventTypeDTO the entity to save
     * @return the persisted entity
     */
    EventTypeDTO save(EventTypeDTO eventTypeDTO);

    /**
     *  Get all the eventTypes.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<EventType> findAll(Pageable pageable);

    /**
     *  Get the "id" eventType.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    EventTypeDTO findOne(Long id);

    /**
     *  Delete the "id" eventType.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
