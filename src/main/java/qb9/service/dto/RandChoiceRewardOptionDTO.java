package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the RandChoiceRewardOption entity.
 */
public class RandChoiceRewardOptionDTO implements Serializable {

    private Long id;

    private BigDecimal p;

    private Integer qty;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;


    private Long rewardId;
    

    private String rewardCode;

    private Long giftItemId;
    

    private String giftItemCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public BigDecimal getP() {
        return p;
    }

    public RandChoiceRewardOptionDTO setP(BigDecimal p) {
        this.p = p;
        return this;
    }
    public Integer getQty() {
        return qty;
    }

    public RandChoiceRewardOptionDTO setQty(Integer qty) {
        this.qty = qty;
        return this;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public RandChoiceRewardOptionDTO setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public RandChoiceRewardOptionDTO setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public Long getRewardId() {
        return rewardId;
    }

    public RandChoiceRewardOptionDTO setRewardId(Long randChoiceRewardId) {
        this.rewardId = randChoiceRewardId;
        return this;
    }


    public String getRewardCode() {
        return rewardCode;
    }

    public RandChoiceRewardOptionDTO setRewardCode(String randChoiceRewardCode) {
        this.rewardCode = randChoiceRewardCode;
        return this;
    }

    public Long getGiftItemId() {
        return giftItemId;
    }

    public RandChoiceRewardOptionDTO setGiftItemId(Long giftItemId) {
        this.giftItemId = giftItemId;
        return this;
    }


    public String getGiftItemCode() {
        return giftItemCode;
    }

    public RandChoiceRewardOptionDTO setGiftItemCode(String giftItemCode) {
        this.giftItemCode = giftItemCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RandChoiceRewardOptionDTO randChoiceRewardOptionDTO = (RandChoiceRewardOptionDTO) o;

        if ( ! Objects.equals(id, randChoiceRewardOptionDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RandChoiceRewardOptionDTO{" +
            "id=" + id +
            ", p='" + p + "'" +
            ", qty='" + qty + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            '}';
    }
}
