package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.RewardStrategyStatus;

/**
 * A DTO for the RandChoiceReward entity.
 */
public class RandChoiceRewardDTO implements Serializable {

    private Long id;

    private String uid;

    private String code;

    private String summary;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private RewardStrategyStatus status;


    private Long goalId;
    

    private String goalCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUid() {
        return uid;
    }

    public RandChoiceRewardDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }
    public String getCode() {
        return code;
    }

    public RandChoiceRewardDTO setCode(String code) {
        this.code = code;
        return this;
    }
    public String getSummary() {
        return summary;
    }

    public RandChoiceRewardDTO setSummary(String summary) {
        this.summary = summary;
        return this;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public RandChoiceRewardDTO setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public RandChoiceRewardDTO setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
    public RewardStrategyStatus getStatus() {
        return status;
    }

    public RandChoiceRewardDTO setStatus(RewardStrategyStatus status) {
        this.status = status;
        return this;
    }

    public Long getGoalId() {
        return goalId;
    }

    public RandChoiceRewardDTO setGoalId(Long goalId) {
        this.goalId = goalId;
        return this;
    }


    public String getGoalCode() {
        return goalCode;
    }

    public RandChoiceRewardDTO setGoalCode(String goalCode) {
        this.goalCode = goalCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RandChoiceRewardDTO randChoiceRewardDTO = (RandChoiceRewardDTO) o;

        if ( ! Objects.equals(id, randChoiceRewardDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RandChoiceRewardDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", summary='" + summary + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
