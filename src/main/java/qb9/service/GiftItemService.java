package qb9.service;

import qb9.domain.GiftItem;
import qb9.web.rest.dto.GiftItemDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing GiftItem.
 */
public interface GiftItemService {

    /**
     * Save a giftItem.
     * 
     * @param giftItemDTO the entity to save
     * @return the persisted entity
     */
    GiftItemDTO save(GiftItemDTO giftItemDTO);

    /**
     *  Get all the giftItems.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<GiftItem> findAll(Pageable pageable);

    /**
     *  Get the "id" giftItem.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    GiftItemDTO findOne(Long id);

    /**
     *  Delete the "id" giftItem.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
