package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.PlayerAccount;
import qb9.service.PlayerAccountService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.PlayerAccountDTO;
import qb9.web.rest.mapper.PlayerAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing PlayerAccount.
 */
@RestController
@RequestMapping("/api")
public class PlayerAccountResource {

    private final Logger log = LoggerFactory.getLogger(PlayerAccountResource.class);
        
    @Inject
    private PlayerAccountService playerAccountService;
    
    @Inject
    private PlayerAccountMapper playerAccountMapper;
    
    /**
     * POST  /player-accounts : Create a new playerAccount.
     *
     * @param playerAccountDTO the playerAccountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new playerAccountDTO, or with status 400 (Bad Request) if the playerAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/player-accounts",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlayerAccountDTO> createPlayerAccount(@RequestBody PlayerAccountDTO playerAccountDTO) throws URISyntaxException {
        log.debug("REST request to save PlayerAccount : {}", playerAccountDTO);
        if (playerAccountDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("playerAccount", "idexists", "A new playerAccount cannot already have an ID")).body(null);
        }
        PlayerAccountDTO result = playerAccountService.save(playerAccountDTO);
        return ResponseEntity.created(new URI("/api/player-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("playerAccount", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /player-accounts : Updates an existing playerAccount.
     *
     * @param playerAccountDTO the playerAccountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated playerAccountDTO,
     * or with status 400 (Bad Request) if the playerAccountDTO is not valid,
     * or with status 500 (Internal Server Error) if the playerAccountDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/player-accounts",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlayerAccountDTO> updatePlayerAccount(@RequestBody PlayerAccountDTO playerAccountDTO) throws URISyntaxException {
        log.debug("REST request to update PlayerAccount : {}", playerAccountDTO);
        if (playerAccountDTO.getId() == null) {
            return createPlayerAccount(playerAccountDTO);
        }
        PlayerAccountDTO result = playerAccountService.save(playerAccountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("playerAccount", playerAccountDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /player-accounts : get all the playerAccounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of playerAccounts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/player-accounts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PlayerAccountDTO>> getAllPlayerAccounts(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PlayerAccounts");
        Page<PlayerAccount> page = playerAccountService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/player-accounts");
        return new ResponseEntity<>(playerAccountMapper.playerAccountsToPlayerAccountDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /player-accounts/:id : get the "id" playerAccount.
     *
     * @param id the id of the playerAccountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the playerAccountDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/player-accounts/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlayerAccountDTO> getPlayerAccount(@PathVariable Long id) {
        log.debug("REST request to get PlayerAccount : {}", id);
        PlayerAccountDTO playerAccountDTO = playerAccountService.findOne(id);
        return Optional.ofNullable(playerAccountDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /player-accounts/:id : delete the "id" playerAccount.
     *
     * @param id the id of the playerAccountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/player-accounts/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePlayerAccount(@PathVariable Long id) {
        log.debug("REST request to delete PlayerAccount : {}", id);
        playerAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("playerAccount", id.toString())).build();
    }

}
