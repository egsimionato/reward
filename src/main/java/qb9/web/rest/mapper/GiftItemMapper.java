package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.GiftItemDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity GiftItem and its DTO GiftItemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GiftItemMapper {

    GiftItemDTO giftItemToGiftItemDTO(GiftItem giftItem);

    List<GiftItemDTO> giftItemsToGiftItemDTOs(List<GiftItem> giftItems);

    GiftItem giftItemDTOToGiftItem(GiftItemDTO giftItemDTO);

    List<GiftItem> giftItemDTOsToGiftItems(List<GiftItemDTO> giftItemDTOs);
}
