package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.GiftFlipDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity GiftFlip and its DTO GiftFlipDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GiftFlipMapper {

    @Mapping(source = "goal.id", target = "goalId")
    @Mapping(source = "goal.code", target = "goalCode")
    GiftFlipDTO giftFlipToGiftFlipDTO(GiftFlip giftFlip);

    List<GiftFlipDTO> giftFlipsToGiftFlipDTOs(List<GiftFlip> giftFlips);

    @Mapping(source = "goalId", target = "goal")
    GiftFlip giftFlipDTOToGiftFlip(GiftFlipDTO giftFlipDTO);

    List<GiftFlip> giftFlipDTOsToGiftFlips(List<GiftFlipDTO> giftFlipDTOs);

    default Goal goalFromId(Long id) {
        if (id == null) {
            return null;
        }
        Goal goal = new Goal();
        goal.setId(id);
        return goal;
    }
}
