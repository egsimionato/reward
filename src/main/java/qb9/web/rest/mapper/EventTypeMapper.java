package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.EventTypeDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity EventType and its DTO EventTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EventTypeMapper {

    EventTypeDTO eventTypeToEventTypeDTO(EventType eventType);

    List<EventTypeDTO> eventTypesToEventTypeDTOs(List<EventType> eventTypes);

    EventType eventTypeDTOToEventType(EventTypeDTO eventTypeDTO);

    List<EventType> eventTypeDTOsToEventTypes(List<EventTypeDTO> eventTypeDTOs);
}
