package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.PlayerAccountDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity PlayerAccount and its DTO PlayerAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PlayerAccountMapper {

    PlayerAccountDTO playerAccountToPlayerAccountDTO(PlayerAccount playerAccount);

    List<PlayerAccountDTO> playerAccountsToPlayerAccountDTOs(List<PlayerAccount> playerAccounts);

    PlayerAccount playerAccountDTOToPlayerAccount(PlayerAccountDTO playerAccountDTO);

    List<PlayerAccount> playerAccountDTOsToPlayerAccounts(List<PlayerAccountDTO> playerAccountDTOs);
}
