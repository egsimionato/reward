package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.FlipRewardDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity FlipReward and its DTO FlipRewardDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FlipRewardMapper {

    @Mapping(source = "flip.id", target = "flipId")
    @Mapping(source = "flip.code", target = "flipCode")
    @Mapping(source = "reward.id", target = "rewardId")
    @Mapping(source = "reward.code", target = "rewardCode")
    FlipRewardDTO flipRewardToFlipRewardDTO(FlipReward flipReward);

    List<FlipRewardDTO> flipRewardsToFlipRewardDTOs(List<FlipReward> flipRewards);

    @Mapping(source = "flipId", target = "flip")
    @Mapping(source = "rewardId", target = "reward")
    FlipReward flipRewardDTOToFlipReward(FlipRewardDTO flipRewardDTO);

    List<FlipReward> flipRewardDTOsToFlipRewards(List<FlipRewardDTO> flipRewardDTOs);

    default GiftFlip giftFlipFromId(Long id) {
        if (id == null) {
            return null;
        }
        GiftFlip giftFlip = new GiftFlip();
        giftFlip.setId(id);
        return giftFlip;
    }

    default GiftItem giftItemFromId(Long id) {
        if (id == null) {
            return null;
        }
        GiftItem giftItem = new GiftItem();
        giftItem.setId(id);
        return giftItem;
    }
}
