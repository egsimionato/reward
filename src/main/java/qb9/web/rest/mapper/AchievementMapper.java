package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.AchievementDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Achievement and its DTO AchievementDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AchievementMapper {

    @Mapping(source = "goal.id", target = "goalId")
    @Mapping(source = "goal.code", target = "goalCode")
    @Mapping(source = "player.id", target = "playerId")
    @Mapping(source = "player.username", target = "playerUsername")
    AchievementDTO achievementToAchievementDTO(Achievement achievement);

    List<AchievementDTO> achievementsToAchievementDTOs(List<Achievement> achievements);

    @Mapping(source = "goalId", target = "goal")
    @Mapping(source = "playerId", target = "player")
    Achievement achievementDTOToAchievement(AchievementDTO achievementDTO);

    List<Achievement> achievementDTOsToAchievements(List<AchievementDTO> achievementDTOs);

    default Goal goalFromId(Long id) {
        if (id == null) {
            return null;
        }
        Goal goal = new Goal();
        goal.setId(id);
        return goal;
    }

    default PlayerAccount playerAccountFromId(Long id) {
        if (id == null) {
            return null;
        }
        PlayerAccount playerAccount = new PlayerAccount();
        playerAccount.setId(id);
        return playerAccount;
    }
}
