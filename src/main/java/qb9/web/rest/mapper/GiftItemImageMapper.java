package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.GiftItemImageDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity GiftItemImage and its DTO GiftItemImageDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GiftItemImageMapper {

    @Mapping(source = "giftItem.id", target = "giftItemId")
    @Mapping(source = "giftItem.code", target = "giftItemCode")
    GiftItemImageDTO giftItemImageToGiftItemImageDTO(GiftItemImage giftItemImage);

    List<GiftItemImageDTO> giftItemImagesToGiftItemImageDTOs(List<GiftItemImage> giftItemImages);

    @Mapping(source = "giftItemId", target = "giftItem")
    GiftItemImage giftItemImageDTOToGiftItemImage(GiftItemImageDTO giftItemImageDTO);

    List<GiftItemImage> giftItemImageDTOsToGiftItemImages(List<GiftItemImageDTO> giftItemImageDTOs);

    default GiftItem giftItemFromId(Long id) {
        if (id == null) {
            return null;
        }
        GiftItem giftItem = new GiftItem();
        giftItem.setId(id);
        return giftItem;
    }
}
