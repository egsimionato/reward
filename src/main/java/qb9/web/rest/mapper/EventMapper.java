package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.EventDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Event and its DTO EventDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EventMapper {

    @Mapping(source = "type.id", target = "typeId")
    @Mapping(source = "type.code", target = "typeCode")
    @Mapping(source = "player.id", target = "playerId")
    @Mapping(source = "player.username", target = "playerUsername")
    EventDTO eventToEventDTO(Event event);

    List<EventDTO> eventsToEventDTOs(List<Event> events);

    @Mapping(source = "typeId", target = "type")
    @Mapping(source = "playerId", target = "player")
    Event eventDTOToEvent(EventDTO eventDTO);

    List<Event> eventDTOsToEvents(List<EventDTO> eventDTOs);

    default EventType eventTypeFromId(Long id) {
        if (id == null) {
            return null;
        }
        EventType eventType = new EventType();
        eventType.setId(id);
        return eventType;
    }

    default PlayerAccount playerAccountFromId(Long id) {
        if (id == null) {
            return null;
        }
        PlayerAccount playerAccount = new PlayerAccount();
        playerAccount.setId(id);
        return playerAccount;
    }
}
