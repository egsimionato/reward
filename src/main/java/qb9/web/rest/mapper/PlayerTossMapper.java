package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.PlayerTossDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity PlayerToss and its DTO PlayerTossDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PlayerTossMapper {

    @Mapping(source = "flip.id", target = "flipId")
    @Mapping(source = "flip.code", target = "flipCode")
    @Mapping(source = "player.id", target = "playerId")
    @Mapping(source = "player.username", target = "playerUsername")
    PlayerTossDTO playerTossToPlayerTossDTO(PlayerToss playerToss);

    List<PlayerTossDTO> playerTossesToPlayerTossDTOs(List<PlayerToss> playerTosses);

    @Mapping(source = "flipId", target = "flip")
    @Mapping(source = "playerId", target = "player")
    PlayerToss playerTossDTOToPlayerToss(PlayerTossDTO playerTossDTO);

    List<PlayerToss> playerTossDTOsToPlayerTosses(List<PlayerTossDTO> playerTossDTOs);

    default GiftFlip giftFlipFromId(Long id) {
        if (id == null) {
            return null;
        }
        GiftFlip giftFlip = new GiftFlip();
        giftFlip.setId(id);
        return giftFlip;
    }

    default PlayerAccount playerAccountFromId(Long id) {
        if (id == null) {
            return null;
        }
        PlayerAccount playerAccount = new PlayerAccount();
        playerAccount.setId(id);
        return playerAccount;
    }
}
