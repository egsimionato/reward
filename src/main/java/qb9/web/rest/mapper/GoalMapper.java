package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.GoalDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Goal and its DTO GoalDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GoalMapper {

    GoalDTO goalToGoalDTO(Goal goal);

    List<GoalDTO> goalsToGoalDTOs(List<Goal> goals);

    Goal goalDTOToGoal(GoalDTO goalDTO);

    List<Goal> goalDTOsToGoals(List<GoalDTO> goalDTOs);
}
