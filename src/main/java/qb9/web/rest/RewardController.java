package qb9.web.rest;

import qb9.util.QB9Random;
import qb9.domain.FlipReward;
import qb9.domain.GiftFlip;
import qb9.domain.Goal;
import qb9.domain.PlayerAccount;
import qb9.domain.PlayerAccount;
import qb9.repository.FlipRewardRepository;
import qb9.repository.GiftFlipRepository;
import qb9.repository.GoalRepository;
import qb9.web.rest.dto.GoalDTO;
import qb9.web.rest.dto.PlayerTossDTO;
import qb9.web.rest.dto.RewardDTO;
import qb9.web.rest.errors.ErrorConstants;
import qb9.web.rest.errors.ErrorDTO;
import qb9.web.rest.mapper.GoalMapper;
import qb9.web.rest.util.HeaderUtil;

import qb9.math.std.StdFlip;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.math.BigDecimal;
import java.lang.Math;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import io.swagger.annotations.*;

@Component
@RestController
@Transactional
public class RewardController {

    private final Logger log = LoggerFactory.getLogger(RewardController.class);

    @Inject
    private GoalRepository goalRepository;

    @Inject
    private GiftFlipRepository flipRepository;

    @Inject
    private FlipRewardRepository rewardRepository;

    @Value("${qb9.reward.turn_limit}")
    private int turnLimit;

    @Value("${qb9.reward.gaturro.give_material_url}")
    private String gatuGiveUrl;

    @Value("${qb9.reward.gaturro.profile_info_url}")
    private String gatuProfileInfoUrl;


    /**
     * POST  /reward : Reward a goal achievement by a player.
     *
     * @param rewardDTO the reward info to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the playerTossDTO information,
     * or with status 400 (Bad Request) if the player or goal are not registred,
     * or status 500 (Internal Server Error) if the reward couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Reward a achievement",
            nickname = "reward",
            notes = "Reward a goal achivement by a player")
    @RequestMapping(value = "/api/reward", method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = PlayerTossDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> reward(
            @ApiParam(value="the reward info to processed.", required=true) @Valid @RequestBody RewardDTO rewardDTO,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) throws URISyntaxException {
        log.debug("REST request to reward achievement");
        try {
            Map mReturn = new HashMap();
            BodyBuilder builder;
            Goal goal = null;
            RestTemplate rt = new RestTemplate();
            Map rVars = new HashMap();
            rVars.put("username", rewardDTO.getPlayerUsername());

            Optional<Goal> opGoal = goalRepository.findOneByCode(rewardDTO.getGoalCode());
            if (opGoal.isPresent()) {
                goal = opGoal.get();
            } else {
                HttpHeaders headers = HeaderUtil.createAlert(
                        "error.goalnotexists", "request=reward;goal="+rewardDTO.getGoalCode());
                ErrorDTO errorDTO = new ErrorDTO(
                        ErrorConstants.ERR_VALIDATION, ErrorConstants.ERR_VALIDATION);
                errorDTO.add("RewardDTO", "goalCode", "goalNotExists");
                return new ResponseEntity<>(errorDTO, headers, HttpStatus.BAD_REQUEST);
            }

            GiftFlip flip = null;
            Optional<GiftFlip> opFlip = flipRepository.findOneByGoal(goal);
            if (opFlip.isPresent()) {
                flip = opFlip.get();
            } else {
                HttpHeaders headers = HeaderUtil.createAlert(
                        "error.norewardconfig", "request=reward;goal="+rewardDTO.getGoalCode());
                ErrorDTO errorDTO = new ErrorDTO(
                        ErrorConstants.ERR_VALIDATION, ErrorConstants.ERR_VALIDATION);
                errorDTO.add("RewardDTO", "goalCode", "goalNotRewardConfig");
                return new ResponseEntity<>(errorDTO, headers, HttpStatus.BAD_REQUEST);
            }

            List<FlipReward> rewards = rewardRepository.findAllByFlip(flip);
            List<Map> mGiveItems = new ArrayList<Map>();
            Map<String,Integer> mRewards = new HashMap<String,Integer>();
            int bTurns = rewardDTO.getRatio().divide(flip.getTurnRatio(), BigDecimal.ROUND_HALF_UP).intValue();
            int turns = Math.min(bTurns, turnLimit);
            if (log.isDebugEnabled()) {
                log.debug("turn: {} => turn_limit: {}, turns_achived {}", turns, turnLimit, bTurns);
            }
            if(turns>0) {
                for (FlipReward reward : rewards) {
                        Map mItem = new HashMap();
                        double p = reward.getP().doubleValue();
                        int qtyTurn = reward.getQty().intValue();
                        int heads = 0;
                        int qtyTot = 0;
                        String code = reward.getReward().getCode();
                        if (p>0) {
                            heads = StdFlip.flip(turns, p);
                            if (heads>0) {
                                qtyTot = heads * qtyTurn;
                                mItem.put("name", code);
                                mItem.put("amount", qtyTot);
                                mRewards.put(code, qtyTot);
                                mGiveItems.add(mItem);
                            }
                        }
                }
                if (mGiveItems.size()>0) {
                    Map<String, Object> giveBody = new HashMap();
                    giveBody.put("materials", mGiveItems);
                    String gatuPost = rt.postForObject(gatuGiveUrl, giveBody, String.class, rVars);
                }
            }
            mReturn.put("rewards", mRewards);

            log.trace("@Z Achievement of {} Goal with {} ratio for {} Account Reward in {} turns with {}",
                        goal.getCode(),
                        rewardDTO.getRatio(),
                        rewardDTO.getPlayerUsername(),
                        turns,
                        mGiveItems);
            return new ResponseEntity<>(mReturn, HttpStatus.OK);
        } catch (Exception ex) {
            log.debug("Exception in REST request to reward achievement ", ex);
            throw ex;
        }

    }
}
