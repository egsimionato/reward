package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.service.RandChoiceRewardOptionService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.service.dto.RandChoiceRewardOptionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing RandChoiceRewardOption.
 */
@RestController
@RequestMapping("/api")
public class RandChoiceRewardOptionResource {

    private final Logger log = LoggerFactory.getLogger(RandChoiceRewardOptionResource.class);
        
    @Inject
    private RandChoiceRewardOptionService randChoiceRewardOptionService;

    /**
     * POST  /rand-choice-reward-options : Create a new randChoiceRewardOption.
     *
     * @param randChoiceRewardOptionDTO the randChoiceRewardOptionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new randChoiceRewardOptionDTO, or with status 400 (Bad Request) if the randChoiceRewardOption has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/rand-choice-reward-options",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RandChoiceRewardOptionDTO> createRandChoiceRewardOption(@RequestBody RandChoiceRewardOptionDTO randChoiceRewardOptionDTO) throws URISyntaxException {
        log.debug("REST request to save RandChoiceRewardOption : {}", randChoiceRewardOptionDTO);
        if (randChoiceRewardOptionDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("randChoiceRewardOption", "idexists", "A new randChoiceRewardOption cannot already have an ID")).body(null);
        }
        RandChoiceRewardOptionDTO result = randChoiceRewardOptionService.save(randChoiceRewardOptionDTO);
        return ResponseEntity.created(new URI("/api/rand-choice-reward-options/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("randChoiceRewardOption", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rand-choice-reward-options : Updates an existing randChoiceRewardOption.
     *
     * @param randChoiceRewardOptionDTO the randChoiceRewardOptionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated randChoiceRewardOptionDTO,
     * or with status 400 (Bad Request) if the randChoiceRewardOptionDTO is not valid,
     * or with status 500 (Internal Server Error) if the randChoiceRewardOptionDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/rand-choice-reward-options",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RandChoiceRewardOptionDTO> updateRandChoiceRewardOption(@RequestBody RandChoiceRewardOptionDTO randChoiceRewardOptionDTO) throws URISyntaxException {
        log.debug("REST request to update RandChoiceRewardOption : {}", randChoiceRewardOptionDTO);
        if (randChoiceRewardOptionDTO.getId() == null) {
            return createRandChoiceRewardOption(randChoiceRewardOptionDTO);
        }
        RandChoiceRewardOptionDTO result = randChoiceRewardOptionService.save(randChoiceRewardOptionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("randChoiceRewardOption", randChoiceRewardOptionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rand-choice-reward-options : get all the randChoiceRewardOptions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of randChoiceRewardOptions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/rand-choice-reward-options",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<RandChoiceRewardOptionDTO>> getAllRandChoiceRewardOptions(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of RandChoiceRewardOptions");
        Page<RandChoiceRewardOptionDTO> page = randChoiceRewardOptionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rand-choice-reward-options");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rand-choice-reward-options/:id : get the "id" randChoiceRewardOption.
     *
     * @param id the id of the randChoiceRewardOptionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the randChoiceRewardOptionDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/rand-choice-reward-options/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RandChoiceRewardOptionDTO> getRandChoiceRewardOption(@PathVariable Long id) {
        log.debug("REST request to get RandChoiceRewardOption : {}", id);
        RandChoiceRewardOptionDTO randChoiceRewardOptionDTO = randChoiceRewardOptionService.findOne(id);
        return Optional.ofNullable(randChoiceRewardOptionDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /rand-choice-reward-options/:id : delete the "id" randChoiceRewardOption.
     *
     * @param id the id of the randChoiceRewardOptionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/rand-choice-reward-options/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteRandChoiceRewardOption(@PathVariable Long id) {
        log.debug("REST request to delete RandChoiceRewardOption : {}", id);
        randChoiceRewardOptionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("randChoiceRewardOption", id.toString())).build();
    }

}
