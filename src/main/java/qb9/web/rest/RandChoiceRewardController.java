package qb9.web.rest;

import qb9.util.QB9Random;
import qb9.domain.RandChoiceReward;
import qb9.domain.RandChoiceRewardOption;
import qb9.domain.Goal;
import qb9.domain.PlayerAccount;
import qb9.domain.PlayerAccount;
import qb9.repository.RandChoiceRewardRepository;
import qb9.repository.RandChoiceRewardOptionRepository;
import qb9.repository.GoalRepository;
import qb9.web.rest.dto.GoalDTO;
import qb9.web.rest.dto.PlayerTossDTO;
import qb9.web.rest.dto.RewardDTO;
import qb9.web.rest.errors.ErrorConstants;
import qb9.web.rest.errors.ErrorDTO;
import qb9.web.rest.mapper.GoalMapper;
import qb9.web.rest.util.HeaderUtil;

import qb9.math.std.StdRandom;

import java.net.URI;
import java.net.URISyntaxException;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.math.BigDecimal;
import java.lang.Math;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import io.swagger.annotations.*;

import org.apache.commons.math3.util.Pair;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@RestController
@Transactional
public class RandChoiceRewardController {

    private final Logger log = LoggerFactory.getLogger(RandChoiceRewardController.class);

    @Inject
    private GoalRepository goalRepository;

    @Inject
    private RandChoiceRewardRepository randChoiceRewardRepository;

    @Inject
    private RandChoiceRewardOptionRepository randChoiceRewardOptionRepository;

    @Value("${qb9.reward.gaturro.give_material_url}")
    private String gatuGiveUrl;

    @Value("${qb9.reward.gaturro.profile_info_url}")
    private String gatuProfileInfoUrl;


    /**
     * POST  /reward : Reward a goal achievement by a player.
     *
     * @param rewardDTO the reward info to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the playerTossDTO information,
     * or with status 400 (Bad Request) if the player or goal are not registred,
     * or status 500 (Internal Server Error) if the reward couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Reward a achievement",
            nickname = "reward",
            notes = "Reward a goal achivement by a player")
    @RequestMapping(value = "/api/reward-choice", method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = PlayerTossDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> reward(
            @ApiParam(value="the reward info to processed.", required=true) @Valid @RequestBody RewardDTO rewardDTO,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) throws URISyntaxException, IOException {
        log.debug("REST request to reward achievement");
        try {
            Map mReturn = new HashMap();
            BodyBuilder builder;
            Goal goal = null;
            RestTemplate rt = new RestTemplate();
            Map rVars = new HashMap();
            rVars.put("username", rewardDTO.getPlayerUsername());

            Optional<Goal> opGoal = goalRepository.findOneByCode(rewardDTO.getGoalCode());
            if (opGoal.isPresent()) {
                goal = opGoal.get();
            } else {
                HttpHeaders headers = HeaderUtil.createAlert(
                        "error.goalnotexists", "request=reward;goal="+rewardDTO.getGoalCode());
                ErrorDTO errorDTO = new ErrorDTO(
                        ErrorConstants.ERR_VALIDATION, ErrorConstants.ERR_VALIDATION);
                errorDTO.add("RewardDTO", "goalCode", "goalNotExists");
                return new ResponseEntity<>(errorDTO, headers, HttpStatus.BAD_REQUEST);
            }

            RandChoiceReward reward = null;
            Optional<RandChoiceReward> opReward = randChoiceRewardRepository.findOneByGoal(goal);
            if (opReward.isPresent()) {
                reward = opReward.get();
            } else {
                ErrorDTO errorDTO = new ErrorDTO(
                        ErrorConstants.ERR_VALIDATION, ErrorConstants.ERR_VALIDATION);
                errorDTO.add("RewardDTO", "goalCode", "goalNotRewardConfig");
                return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
            }

            List<RandChoiceRewardOption> rewardOptions = randChoiceRewardOptionRepository.findAllByReward(reward);
            Collections.shuffle(rewardOptions);

            List<Pair<RandChoiceRewardOption, Double>> pairs = new ArrayList<Pair<RandChoiceRewardOption, Double>>();
            for (RandChoiceRewardOption rewardOption: rewardOptions) {
                pairs.add(new Pair<>(rewardOption, Double.valueOf(rewardOption.getP().doubleValue())));
            }
            RandChoiceRewardOption rewardOptionSelected = StdRandom.randomChoice(pairs);
            //RandChoiceRewardOption rewardOptionSelected = rewardOptions.get(0);

            Map<String,Integer> mRewards = new HashMap<String,Integer>();
            List<Map> mGiveItems = new ArrayList<Map>();
            Map<String, Object> giveBody = new HashMap();
            Map mItem = new HashMap();
            mItem.put("name", rewardOptionSelected.getGiftItem().getCode());
            mItem.put("amount", rewardOptionSelected.getQty());
            mRewards.put(rewardOptionSelected.getGiftItem().getCode(), rewardOptionSelected.getQty());
            mGiveItems.add(mItem);
            giveBody.put("materials", mGiveItems);
            String gatuPost = rt.postForObject(gatuGiveUrl, giveBody, String.class, rVars);
            mReturn.put("rewards", mRewards);
            ObjectMapper jsonMapper = new ObjectMapper();
            JsonNode rootNode = jsonMapper.readTree(gatuPost);
            log.debug("@Z GUSTAVO :P {}", rootNode.path("materials"));

            mReturn.put("rewards", mRewards);
            mReturn.put("more", rootNode.path("materials"));

            log.trace("@Z Achievement of {} Goal with {} ratio for {} Account Reward with {}",
                        goal.getCode(),
                        rewardDTO.getRatio(),
                        rewardDTO.getPlayerUsername(),
                        mGiveItems);
            return new ResponseEntity<>(mReturn, HttpStatus.OK);
        } catch (Exception ex) {
            log.debug("Exception in REST request to reward achievement ", ex);
            throw ex;
        }

    }


}
