package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.FlipReward;
import qb9.service.FlipRewardService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.FlipRewardDTO;
import qb9.web.rest.mapper.FlipRewardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing FlipReward.
 */
@RestController
@RequestMapping("/api")
public class FlipRewardResource {

    private final Logger log = LoggerFactory.getLogger(FlipRewardResource.class);
        
    @Inject
    private FlipRewardService flipRewardService;
    
    @Inject
    private FlipRewardMapper flipRewardMapper;
    
    /**
     * POST  /flip-rewards : Create a new flipReward.
     *
     * @param flipRewardDTO the flipRewardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new flipRewardDTO, or with status 400 (Bad Request) if the flipReward has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/flip-rewards",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FlipRewardDTO> createFlipReward(@RequestBody FlipRewardDTO flipRewardDTO) throws URISyntaxException {
        log.debug("REST request to save FlipReward : {}", flipRewardDTO);
        if (flipRewardDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("flipReward", "idexists", "A new flipReward cannot already have an ID")).body(null);
        }
        FlipRewardDTO result = flipRewardService.save(flipRewardDTO);
        return ResponseEntity.created(new URI("/api/flip-rewards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("flipReward", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /flip-rewards : Updates an existing flipReward.
     *
     * @param flipRewardDTO the flipRewardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated flipRewardDTO,
     * or with status 400 (Bad Request) if the flipRewardDTO is not valid,
     * or with status 500 (Internal Server Error) if the flipRewardDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/flip-rewards",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FlipRewardDTO> updateFlipReward(@RequestBody FlipRewardDTO flipRewardDTO) throws URISyntaxException {
        log.debug("REST request to update FlipReward : {}", flipRewardDTO);
        if (flipRewardDTO.getId() == null) {
            return createFlipReward(flipRewardDTO);
        }
        FlipRewardDTO result = flipRewardService.save(flipRewardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("flipReward", flipRewardDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /flip-rewards : get all the flipRewards.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of flipRewards in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/flip-rewards",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<FlipRewardDTO>> getAllFlipRewards(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of FlipRewards");
        Page<FlipReward> page = flipRewardService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/flip-rewards");
        return new ResponseEntity<>(flipRewardMapper.flipRewardsToFlipRewardDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /flip-rewards/:id : get the "id" flipReward.
     *
     * @param id the id of the flipRewardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the flipRewardDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/flip-rewards/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FlipRewardDTO> getFlipReward(@PathVariable Long id) {
        log.debug("REST request to get FlipReward : {}", id);
        FlipRewardDTO flipRewardDTO = flipRewardService.findOne(id);
        return Optional.ofNullable(flipRewardDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /flip-rewards/:id : delete the "id" flipReward.
     *
     * @param id the id of the flipRewardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/flip-rewards/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFlipReward(@PathVariable Long id) {
        log.debug("REST request to delete FlipReward : {}", id);
        flipRewardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("flipReward", id.toString())).build();
    }

}
