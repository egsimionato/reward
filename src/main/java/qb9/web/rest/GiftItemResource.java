package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.GiftItem;
import qb9.service.GiftItemService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.GiftItemDTO;
import qb9.web.rest.mapper.GiftItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing GiftItem.
 */
@RestController
@RequestMapping("/api")
public class GiftItemResource {

    private final Logger log = LoggerFactory.getLogger(GiftItemResource.class);
        
    @Inject
    private GiftItemService giftItemService;
    
    @Inject
    private GiftItemMapper giftItemMapper;
    
    /**
     * POST  /gift-items : Create a new giftItem.
     *
     * @param giftItemDTO the giftItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new giftItemDTO, or with status 400 (Bad Request) if the giftItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-items",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftItemDTO> createGiftItem(@RequestBody GiftItemDTO giftItemDTO) throws URISyntaxException {
        log.debug("REST request to save GiftItem : {}", giftItemDTO);
        if (giftItemDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("giftItem", "idexists", "A new giftItem cannot already have an ID")).body(null);
        }
        GiftItemDTO result = giftItemService.save(giftItemDTO);
        return ResponseEntity.created(new URI("/api/gift-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("giftItem", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gift-items : Updates an existing giftItem.
     *
     * @param giftItemDTO the giftItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated giftItemDTO,
     * or with status 400 (Bad Request) if the giftItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the giftItemDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-items",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftItemDTO> updateGiftItem(@RequestBody GiftItemDTO giftItemDTO) throws URISyntaxException {
        log.debug("REST request to update GiftItem : {}", giftItemDTO);
        if (giftItemDTO.getId() == null) {
            return createGiftItem(giftItemDTO);
        }
        GiftItemDTO result = giftItemService.save(giftItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("giftItem", giftItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gift-items : get all the giftItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of giftItems in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/gift-items",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GiftItemDTO>> getAllGiftItems(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of GiftItems");
        Page<GiftItem> page = giftItemService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gift-items");
        return new ResponseEntity<>(giftItemMapper.giftItemsToGiftItemDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /gift-items/:id : get the "id" giftItem.
     *
     * @param id the id of the giftItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the giftItemDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/gift-items/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftItemDTO> getGiftItem(@PathVariable Long id) {
        log.debug("REST request to get GiftItem : {}", id);
        GiftItemDTO giftItemDTO = giftItemService.findOne(id);
        return Optional.ofNullable(giftItemDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /gift-items/:id : delete the "id" giftItem.
     *
     * @param id the id of the giftItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/gift-items/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGiftItem(@PathVariable Long id) {
        log.debug("REST request to delete GiftItem : {}", id);
        giftItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("giftItem", id.toString())).build();
    }

}
