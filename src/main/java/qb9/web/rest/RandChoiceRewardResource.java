package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.service.RandChoiceRewardService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.service.dto.RandChoiceRewardDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing RandChoiceReward.
 */
@RestController
@RequestMapping("/api")
public class RandChoiceRewardResource {

    private final Logger log = LoggerFactory.getLogger(RandChoiceRewardResource.class);
        
    @Inject
    private RandChoiceRewardService randChoiceRewardService;

    /**
     * POST  /rand-choice-rewards : Create a new randChoiceReward.
     *
     * @param randChoiceRewardDTO the randChoiceRewardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new randChoiceRewardDTO, or with status 400 (Bad Request) if the randChoiceReward has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/rand-choice-rewards",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RandChoiceRewardDTO> createRandChoiceReward(@RequestBody RandChoiceRewardDTO randChoiceRewardDTO) throws URISyntaxException {
        log.debug("REST request to save RandChoiceReward : {}", randChoiceRewardDTO);
        if (randChoiceRewardDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("randChoiceReward", "idexists", "A new randChoiceReward cannot already have an ID")).body(null);
        }
        RandChoiceRewardDTO result = randChoiceRewardService.save(randChoiceRewardDTO);
        return ResponseEntity.created(new URI("/api/rand-choice-rewards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("randChoiceReward", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rand-choice-rewards : Updates an existing randChoiceReward.
     *
     * @param randChoiceRewardDTO the randChoiceRewardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated randChoiceRewardDTO,
     * or with status 400 (Bad Request) if the randChoiceRewardDTO is not valid,
     * or with status 500 (Internal Server Error) if the randChoiceRewardDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/rand-choice-rewards",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RandChoiceRewardDTO> updateRandChoiceReward(@RequestBody RandChoiceRewardDTO randChoiceRewardDTO) throws URISyntaxException {
        log.debug("REST request to update RandChoiceReward : {}", randChoiceRewardDTO);
        if (randChoiceRewardDTO.getId() == null) {
            return createRandChoiceReward(randChoiceRewardDTO);
        }
        RandChoiceRewardDTO result = randChoiceRewardService.save(randChoiceRewardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("randChoiceReward", randChoiceRewardDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rand-choice-rewards : get all the randChoiceRewards.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of randChoiceRewards in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/rand-choice-rewards",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<RandChoiceRewardDTO>> getAllRandChoiceRewards(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of RandChoiceRewards");
        Page<RandChoiceRewardDTO> page = randChoiceRewardService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rand-choice-rewards");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rand-choice-rewards/:id : get the "id" randChoiceReward.
     *
     * @param id the id of the randChoiceRewardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the randChoiceRewardDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/rand-choice-rewards/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RandChoiceRewardDTO> getRandChoiceReward(@PathVariable Long id) {
        log.debug("REST request to get RandChoiceReward : {}", id);
        RandChoiceRewardDTO randChoiceRewardDTO = randChoiceRewardService.findOne(id);
        return Optional.ofNullable(randChoiceRewardDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /rand-choice-rewards/:id : delete the "id" randChoiceReward.
     *
     * @param id the id of the randChoiceRewardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/rand-choice-rewards/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteRandChoiceReward(@PathVariable Long id) {
        log.debug("REST request to delete RandChoiceReward : {}", id);
        randChoiceRewardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("randChoiceReward", id.toString())).build();
    }

}
