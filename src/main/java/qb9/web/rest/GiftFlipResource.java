package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.GiftFlip;
import qb9.service.GiftFlipService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.GiftFlipDTO;
import qb9.web.rest.mapper.GiftFlipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing GiftFlip.
 */
@RestController
@RequestMapping("/api")
public class GiftFlipResource {

    private final Logger log = LoggerFactory.getLogger(GiftFlipResource.class);
        
    @Inject
    private GiftFlipService giftFlipService;
    
    @Inject
    private GiftFlipMapper giftFlipMapper;
    
    /**
     * POST  /gift-flips : Create a new giftFlip.
     *
     * @param giftFlipDTO the giftFlipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new giftFlipDTO, or with status 400 (Bad Request) if the giftFlip has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-flips",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftFlipDTO> createGiftFlip(@RequestBody GiftFlipDTO giftFlipDTO) throws URISyntaxException {
        log.debug("REST request to save GiftFlip : {}", giftFlipDTO);
        if (giftFlipDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("giftFlip", "idexists", "A new giftFlip cannot already have an ID")).body(null);
        }
        GiftFlipDTO result = giftFlipService.save(giftFlipDTO);
        return ResponseEntity.created(new URI("/api/gift-flips/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("giftFlip", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gift-flips : Updates an existing giftFlip.
     *
     * @param giftFlipDTO the giftFlipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated giftFlipDTO,
     * or with status 400 (Bad Request) if the giftFlipDTO is not valid,
     * or with status 500 (Internal Server Error) if the giftFlipDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-flips",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftFlipDTO> updateGiftFlip(@RequestBody GiftFlipDTO giftFlipDTO) throws URISyntaxException {
        log.debug("REST request to update GiftFlip : {}", giftFlipDTO);
        if (giftFlipDTO.getId() == null) {
            return createGiftFlip(giftFlipDTO);
        }
        GiftFlipDTO result = giftFlipService.save(giftFlipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("giftFlip", giftFlipDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gift-flips : get all the giftFlips.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of giftFlips in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/gift-flips",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GiftFlipDTO>> getAllGiftFlips(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of GiftFlips");
        Page<GiftFlip> page = giftFlipService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gift-flips");
        return new ResponseEntity<>(giftFlipMapper.giftFlipsToGiftFlipDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /gift-flips/:id : get the "id" giftFlip.
     *
     * @param id the id of the giftFlipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the giftFlipDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/gift-flips/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftFlipDTO> getGiftFlip(@PathVariable Long id) {
        log.debug("REST request to get GiftFlip : {}", id);
        GiftFlipDTO giftFlipDTO = giftFlipService.findOne(id);
        return Optional.ofNullable(giftFlipDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /gift-flips/:id : delete the "id" giftFlip.
     *
     * @param id the id of the giftFlipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/gift-flips/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGiftFlip(@PathVariable Long id) {
        log.debug("REST request to delete GiftFlip : {}", id);
        giftFlipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("giftFlip", id.toString())).build();
    }

}
