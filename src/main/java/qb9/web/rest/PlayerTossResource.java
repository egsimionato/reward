package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.PlayerToss;
import qb9.service.PlayerTossService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.PlayerTossDTO;
import qb9.web.rest.mapper.PlayerTossMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing PlayerToss.
 */
@RestController
@RequestMapping("/api")
public class PlayerTossResource {

    private final Logger log = LoggerFactory.getLogger(PlayerTossResource.class);
        
    @Inject
    private PlayerTossService playerTossService;
    
    @Inject
    private PlayerTossMapper playerTossMapper;
    
    /**
     * POST  /player-tosses : Create a new playerToss.
     *
     * @param playerTossDTO the playerTossDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new playerTossDTO, or with status 400 (Bad Request) if the playerToss has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/player-tosses",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlayerTossDTO> createPlayerToss(@RequestBody PlayerTossDTO playerTossDTO) throws URISyntaxException {
        log.debug("REST request to save PlayerToss : {}", playerTossDTO);
        if (playerTossDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("playerToss", "idexists", "A new playerToss cannot already have an ID")).body(null);
        }
        PlayerTossDTO result = playerTossService.save(playerTossDTO);
        return ResponseEntity.created(new URI("/api/player-tosses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("playerToss", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /player-tosses : Updates an existing playerToss.
     *
     * @param playerTossDTO the playerTossDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated playerTossDTO,
     * or with status 400 (Bad Request) if the playerTossDTO is not valid,
     * or with status 500 (Internal Server Error) if the playerTossDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/player-tosses",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlayerTossDTO> updatePlayerToss(@RequestBody PlayerTossDTO playerTossDTO) throws URISyntaxException {
        log.debug("REST request to update PlayerToss : {}", playerTossDTO);
        if (playerTossDTO.getId() == null) {
            return createPlayerToss(playerTossDTO);
        }
        PlayerTossDTO result = playerTossService.save(playerTossDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("playerToss", playerTossDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /player-tosses : get all the playerTosses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of playerTosses in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/player-tosses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PlayerTossDTO>> getAllPlayerTosses(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PlayerTosses");
        Page<PlayerToss> page = playerTossService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/player-tosses");
        return new ResponseEntity<>(playerTossMapper.playerTossesToPlayerTossDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /player-tosses/:id : get the "id" playerToss.
     *
     * @param id the id of the playerTossDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the playerTossDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/player-tosses/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlayerTossDTO> getPlayerToss(@PathVariable Long id) {
        log.debug("REST request to get PlayerToss : {}", id);
        PlayerTossDTO playerTossDTO = playerTossService.findOne(id);
        return Optional.ofNullable(playerTossDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /player-tosses/:id : delete the "id" playerToss.
     *
     * @param id the id of the playerTossDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/player-tosses/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePlayerToss(@PathVariable Long id) {
        log.debug("REST request to delete PlayerToss : {}", id);
        playerTossService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("playerToss", id.toString())).build();
    }

}
