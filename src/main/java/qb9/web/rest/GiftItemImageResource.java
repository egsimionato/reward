package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.GiftItemImage;
import qb9.service.GiftItemImageService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.GiftItemImageDTO;
import qb9.web.rest.mapper.GiftItemImageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing GiftItemImage.
 */
@RestController
@RequestMapping("/api")
public class GiftItemImageResource {

    private final Logger log = LoggerFactory.getLogger(GiftItemImageResource.class);
        
    @Inject
    private GiftItemImageService giftItemImageService;
    
    @Inject
    private GiftItemImageMapper giftItemImageMapper;
    
    /**
     * POST  /gift-item-images : Create a new giftItemImage.
     *
     * @param giftItemImageDTO the giftItemImageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new giftItemImageDTO, or with status 400 (Bad Request) if the giftItemImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-item-images",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftItemImageDTO> createGiftItemImage(@RequestBody GiftItemImageDTO giftItemImageDTO) throws URISyntaxException {
        log.debug("REST request to save GiftItemImage : {}", giftItemImageDTO);
        if (giftItemImageDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("giftItemImage", "idexists", "A new giftItemImage cannot already have an ID")).body(null);
        }
        GiftItemImageDTO result = giftItemImageService.save(giftItemImageDTO);
        return ResponseEntity.created(new URI("/api/gift-item-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("giftItemImage", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gift-item-images : Updates an existing giftItemImage.
     *
     * @param giftItemImageDTO the giftItemImageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated giftItemImageDTO,
     * or with status 400 (Bad Request) if the giftItemImageDTO is not valid,
     * or with status 500 (Internal Server Error) if the giftItemImageDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-item-images",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftItemImageDTO> updateGiftItemImage(@RequestBody GiftItemImageDTO giftItemImageDTO) throws URISyntaxException {
        log.debug("REST request to update GiftItemImage : {}", giftItemImageDTO);
        if (giftItemImageDTO.getId() == null) {
            return createGiftItemImage(giftItemImageDTO);
        }
        GiftItemImageDTO result = giftItemImageService.save(giftItemImageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("giftItemImage", giftItemImageDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gift-item-images : get all the giftItemImages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of giftItemImages in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/gift-item-images",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GiftItemImageDTO>> getAllGiftItemImages(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of GiftItemImages");
        Page<GiftItemImage> page = giftItemImageService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gift-item-images");
        return new ResponseEntity<>(giftItemImageMapper.giftItemImagesToGiftItemImageDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /gift-item-images/:id : get the "id" giftItemImage.
     *
     * @param id the id of the giftItemImageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the giftItemImageDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/gift-item-images/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftItemImageDTO> getGiftItemImage(@PathVariable Long id) {
        log.debug("REST request to get GiftItemImage : {}", id);
        GiftItemImageDTO giftItemImageDTO = giftItemImageService.findOne(id);
        return Optional.ofNullable(giftItemImageDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /gift-item-images/:id : delete the "id" giftItemImage.
     *
     * @param id the id of the giftItemImageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/gift-item-images/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGiftItemImage(@PathVariable Long id) {
        log.debug("REST request to delete GiftItemImage : {}", id);
        giftItemImageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("giftItemImage", id.toString())).build();
    }

}
