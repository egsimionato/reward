package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.Achievement;
import qb9.service.AchievementService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.AchievementDTO;
import qb9.web.rest.mapper.AchievementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Achievement.
 */
@RestController
@RequestMapping("/api")
public class AchievementResource {

    private final Logger log = LoggerFactory.getLogger(AchievementResource.class);
        
    @Inject
    private AchievementService achievementService;
    
    @Inject
    private AchievementMapper achievementMapper;
    
    /**
     * POST  /achievements : Create a new achievement.
     *
     * @param achievementDTO the achievementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new achievementDTO, or with status 400 (Bad Request) if the achievement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/achievements",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AchievementDTO> createAchievement(@RequestBody AchievementDTO achievementDTO) throws URISyntaxException {
        log.debug("REST request to save Achievement : {}", achievementDTO);
        if (achievementDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("achievement", "idexists", "A new achievement cannot already have an ID")).body(null);
        }
        AchievementDTO result = achievementService.save(achievementDTO);
        return ResponseEntity.created(new URI("/api/achievements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("achievement", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /achievements : Updates an existing achievement.
     *
     * @param achievementDTO the achievementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated achievementDTO,
     * or with status 400 (Bad Request) if the achievementDTO is not valid,
     * or with status 500 (Internal Server Error) if the achievementDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/achievements",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AchievementDTO> updateAchievement(@RequestBody AchievementDTO achievementDTO) throws URISyntaxException {
        log.debug("REST request to update Achievement : {}", achievementDTO);
        if (achievementDTO.getId() == null) {
            return createAchievement(achievementDTO);
        }
        AchievementDTO result = achievementService.save(achievementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("achievement", achievementDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /achievements : get all the achievements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of achievements in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/achievements",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<AchievementDTO>> getAllAchievements(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Achievements");
        Page<Achievement> page = achievementService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/achievements");
        return new ResponseEntity<>(achievementMapper.achievementsToAchievementDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /achievements/:id : get the "id" achievement.
     *
     * @param id the id of the achievementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the achievementDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/achievements/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AchievementDTO> getAchievement(@PathVariable Long id) {
        log.debug("REST request to get Achievement : {}", id);
        AchievementDTO achievementDTO = achievementService.findOne(id);
        return Optional.ofNullable(achievementDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /achievements/:id : delete the "id" achievement.
     *
     * @param id the id of the achievementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/achievements/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAchievement(@PathVariable Long id) {
        log.debug("REST request to delete Achievement : {}", id);
        achievementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("achievement", id.toString())).build();
    }

}
