package qb9.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.TossStatus;

/**
 * A DTO for the PlayerToss entity.
 */
public class PlayerTossDTO implements Serializable {

    private Long id;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private ZonedDateTime sendedAt;

    private ZonedDateTime processedAt;

    private TossStatus status;

    private String result;


    private Long flipId;
    

    private String flipCode;

    private Long playerId;
    

    private String playerUsername;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    public ZonedDateTime getSendedAt() {
        return sendedAt;
    }

    public void setSendedAt(ZonedDateTime sendedAt) {
        this.sendedAt = sendedAt;
    }
    public ZonedDateTime getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(ZonedDateTime processedAt) {
        this.processedAt = processedAt;
    }
    public TossStatus getStatus() {
        return status;
    }

    public void setStatus(TossStatus status) {
        this.status = status;
    }
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getFlipId() {
        return flipId;
    }

    public void setFlipId(Long giftFlipId) {
        this.flipId = giftFlipId;
    }


    public String getFlipCode() {
        return flipCode;
    }

    public void setFlipCode(String giftFlipCode) {
        this.flipCode = giftFlipCode;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerAccountId) {
        this.playerId = playerAccountId;
    }


    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerAccountUsername) {
        this.playerUsername = playerAccountUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlayerTossDTO playerTossDTO = (PlayerTossDTO) o;

        if ( ! Objects.equals(id, playerTossDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PlayerTossDTO{" +
            "id=" + id +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", sendedAt='" + sendedAt + "'" +
            ", processedAt='" + processedAt + "'" +
            ", status='" + status + "'" +
            ", result='" + result + "'" +
            '}';
    }
}
