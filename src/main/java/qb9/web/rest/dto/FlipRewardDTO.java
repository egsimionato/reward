package qb9.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the FlipReward entity.
 */
public class FlipRewardDTO implements Serializable {

    private Long id;

    private BigDecimal p;

    private Integer qty;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;


    private Long flipId;
    

    private String flipCode;

    private Long rewardId;
    

    private String rewardCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public BigDecimal getP() {
        return p;
    }

    public void setP(BigDecimal p) {
        this.p = p;
    }
    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getFlipId() {
        return flipId;
    }

    public void setFlipId(Long giftFlipId) {
        this.flipId = giftFlipId;
    }


    public String getFlipCode() {
        return flipCode;
    }

    public void setFlipCode(String giftFlipCode) {
        this.flipCode = giftFlipCode;
    }

    public Long getRewardId() {
        return rewardId;
    }

    public void setRewardId(Long giftItemId) {
        this.rewardId = giftItemId;
    }


    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String giftItemCode) {
        this.rewardCode = giftItemCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FlipRewardDTO flipRewardDTO = (FlipRewardDTO) o;

        if ( ! Objects.equals(id, flipRewardDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FlipRewardDTO{" +
            "id=" + id +
            ", p='" + p + "'" +
            ", qty='" + qty + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            '}';
    }
}
