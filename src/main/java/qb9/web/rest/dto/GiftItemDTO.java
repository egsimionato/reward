package qb9.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import qb9.domain.enumeration.GiftStatus;

/**
 * A DTO for the GiftItem entity.
 */
public class GiftItemDTO implements Serializable {

    private Long id;

    private String uid;

    private String code;

    private String description;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private GiftStatus status;

    private Integer unitPack;

    private String imageSrc;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    public GiftStatus getStatus() {
        return status;
    }

    public void setStatus(GiftStatus status) {
        this.status = status;
    }
    public Integer getUnitPack() {
        return unitPack;
    }

    public void setUnitPack(Integer unitPack) {
        this.unitPack = unitPack;
    }
    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GiftItemDTO giftItemDTO = (GiftItemDTO) o;

        if ( ! Objects.equals(id, giftItemDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GiftItemDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", description='" + description + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            ", unitPack='" + unitPack + "'" +
            ", imageSrc='" + imageSrc + "'" +
            '}';
    }
}
