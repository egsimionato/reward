package qb9.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.AchievementStatus;

/**
 * A DTO for the Achievement entity.
 */
public class AchievementDTO implements Serializable {

    private Long id;

    private ZonedDateTime createdAt;

    private ZonedDateTime processedAt;

    private AchievementStatus status;

    private BigDecimal ratio;


    private Long goalId;
    

    private String goalCode;

    private Long playerId;
    

    private String playerUsername;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public ZonedDateTime getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(ZonedDateTime processedAt) {
        this.processedAt = processedAt;
    }
    public AchievementStatus getStatus() {
        return status;
    }

    public void setStatus(AchievementStatus status) {
        this.status = status;
    }
    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }


    public String getGoalCode() {
        return goalCode;
    }

    public void setGoalCode(String goalCode) {
        this.goalCode = goalCode;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerAccountId) {
        this.playerId = playerAccountId;
    }


    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerAccountUsername) {
        this.playerUsername = playerAccountUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AchievementDTO achievementDTO = (AchievementDTO) o;

        if ( ! Objects.equals(id, achievementDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AchievementDTO{" +
            "id=" + id +
            ", createdAt='" + createdAt + "'" +
            ", processedAt='" + processedAt + "'" +
            ", status='" + status + "'" +
            ", ratio='" + ratio + "'" +
            '}';
    }
}
