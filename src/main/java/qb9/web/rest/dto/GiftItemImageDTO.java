package qb9.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the GiftItemImage entity.
 */
public class GiftItemImageDTO implements Serializable {

    private Long id;

    private String uuid;

    private ZonedDateTime created_at;

    private ZonedDateTime updated_at;

    private String src;


    private Long giftItemId;
    

    private String giftItemCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    public ZonedDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(ZonedDateTime created_at) {
        this.created_at = created_at;
    }
    public ZonedDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(ZonedDateTime updated_at) {
        this.updated_at = updated_at;
    }
    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Long getGiftItemId() {
        return giftItemId;
    }

    public void setGiftItemId(Long giftItemId) {
        this.giftItemId = giftItemId;
    }


    public String getGiftItemCode() {
        return giftItemCode;
    }

    public void setGiftItemCode(String giftItemCode) {
        this.giftItemCode = giftItemCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GiftItemImageDTO giftItemImageDTO = (GiftItemImageDTO) o;

        if ( ! Objects.equals(id, giftItemImageDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GiftItemImageDTO{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", created_at='" + created_at + "'" +
            ", updated_at='" + updated_at + "'" +
            ", src='" + src + "'" +
            '}';
    }
}
