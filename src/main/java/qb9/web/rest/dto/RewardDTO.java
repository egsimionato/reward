package qb9.web.rest.dto;

import qb9.config.Constants;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * A DTO representing a reward request.
 */
public class RewardDTO {

    @NotNull
    @Size(min = 1, max = 50)
    private String playerUsername;

    @NotNull
    @Size(min = 1, max = 50)
    private String goalCode;

    @NotNull
    @Min(value=0)
    private BigDecimal ratio;



    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public String getGoalCode() {
        return goalCode;
    }

    public void setGoalCode(String goalCode) {
        this.goalCode = goalCode;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }


    @Override
    public String toString() {
        return "RewardDTO{" +
            "playerUsername='" + playerUsername + '\'' +
            ", goalCode='" + goalCode + '\'' +
            ", ratio=" + ratio +
            '}';
    }
}
