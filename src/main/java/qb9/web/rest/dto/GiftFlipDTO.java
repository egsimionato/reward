package qb9.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.FlipStatus;

/**
 * A DTO for the GiftFlip entity.
 */
public class GiftFlipDTO implements Serializable {

    private Long id;

    private String code;

    private String summary;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private FlipStatus status;

    private BigDecimal turnRatio;


    private Long goalId;
    

    private String goalCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    public FlipStatus getStatus() {
        return status;
    }

    public void setStatus(FlipStatus status) {
        this.status = status;
    }
    public BigDecimal getTurnRatio() {
        return turnRatio;
    }

    public void setTurnRatio(BigDecimal turnRatio) {
        this.turnRatio = turnRatio;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }


    public String getGoalCode() {
        return goalCode;
    }

    public void setGoalCode(String goalCode) {
        this.goalCode = goalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GiftFlipDTO giftFlipDTO = (GiftFlipDTO) o;

        if ( ! Objects.equals(id, giftFlipDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GiftFlipDTO{" +
            "id=" + id +
            ", code='" + code + "'" +
            ", summary='" + summary + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            ", turnRatio='" + turnRatio + "'" +
            '}';
    }
}
