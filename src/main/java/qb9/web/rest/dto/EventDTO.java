package qb9.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.EventStatus;

/**
 * A DTO for the Event entity.
 */
public class EventDTO implements Serializable {

    private Long id;

    private String code;

    private ZonedDateTime createdAt;

    private ZonedDateTime processedAt;

    private EventStatus status;

    private BigDecimal ratio;


    private Long typeId;
    

    private String typeCode;

    private Long playerId;
    

    private String playerUsername;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public ZonedDateTime getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(ZonedDateTime processedAt) {
        this.processedAt = processedAt;
    }
    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }
    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long eventTypeId) {
        this.typeId = eventTypeId;
    }


    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String eventTypeCode) {
        this.typeCode = eventTypeCode;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerAccountId) {
        this.playerId = playerAccountId;
    }


    public String getPlayerUsername() {
        return playerUsername;
    }

    public void setPlayerUsername(String playerAccountUsername) {
        this.playerUsername = playerAccountUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventDTO eventDTO = (EventDTO) o;

        if ( ! Objects.equals(id, eventDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EventDTO{" +
            "id=" + id +
            ", code='" + code + "'" +
            ", createdAt='" + createdAt + "'" +
            ", processedAt='" + processedAt + "'" +
            ", status='" + status + "'" +
            ", ratio='" + ratio + "'" +
            '}';
    }
}
