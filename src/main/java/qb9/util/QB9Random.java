/********************************************
 * QB9Random.java
 *
 * A library of static methods to generate pseudo-random numbers from
 * a bernoulli distribution.
 *
 * Remark
 * -------
 *  - Reliees on randomness of nextDouble() method in java.util.Random
 *    to generate pseudorandom numbers in [0, 1).
 *
 ********************************************/
package qb9.util;

import static qb9.math.std.StdRandom.*;

/**
 * The {@code QB9Random} class provides static methods for generating
 * random number from Bernoulli distribution.
 *
 **/
public final class QB9Random {

    private QB9Random() {}

}
