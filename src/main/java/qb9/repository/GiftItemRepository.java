package qb9.repository;

import qb9.domain.GiftItem;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the GiftItem entity.
 */
@SuppressWarnings("unused")
public interface GiftItemRepository extends JpaRepository<GiftItem,Long> {

}
