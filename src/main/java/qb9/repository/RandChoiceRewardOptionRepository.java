package qb9.repository;

import qb9.domain.RandChoiceReward;
import qb9.domain.RandChoiceRewardOption;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the RandChoiceRewardOption entity.
 */
@SuppressWarnings("unused")
public interface RandChoiceRewardOptionRepository extends JpaRepository<RandChoiceRewardOption,Long> {

    @Cacheable(value = "qb9.domain.RandChoiceRewardOption")
    List<RandChoiceRewardOption> findAllByReward(RandChoiceReward reward);

}
