package qb9.repository;

import qb9.domain.PlayerAccount;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PlayerAccount entity.
 */
@SuppressWarnings("unused")
public interface PlayerAccountRepository extends JpaRepository<PlayerAccount,Long> {

}
