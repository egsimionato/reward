package qb9.repository;

import qb9.domain.GiftItemImage;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the GiftItemImage entity.
 */
@SuppressWarnings("unused")
public interface GiftItemImageRepository extends JpaRepository<GiftItemImage,Long> {

}
