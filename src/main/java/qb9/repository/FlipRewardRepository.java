package qb9.repository;

import qb9.domain.FlipReward;
import qb9.domain.GiftFlip;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the FlipReward entity.
 */
@SuppressWarnings("unused")
public interface FlipRewardRepository extends JpaRepository<FlipReward,Long> {

    @Cacheable(value = "qb9.domain.FlipReward")	
    List<FlipReward> findAllByFlip(GiftFlip flip);

}
