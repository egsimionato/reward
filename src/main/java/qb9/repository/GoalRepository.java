package qb9.repository;

import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import qb9.domain.Goal;


/**
 * Spring Data JPA repository for the Goal entity.
 */
@SuppressWarnings("unused")
public interface GoalRepository extends JpaRepository<Goal,Long> {

    Optional<Goal> findOneById(Long goalId);

    @Cacheable(value = "qb9.domain.Goal")    
    Optional<Goal> findOneByCode(String code);

}
