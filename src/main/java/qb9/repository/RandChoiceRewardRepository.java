package qb9.repository;

import qb9.domain.RandChoiceReward;
import qb9.domain.Goal;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the RandChoiceReward entity.
 */
@SuppressWarnings("unused")
public interface RandChoiceRewardRepository extends JpaRepository<RandChoiceReward,Long> {

    @Cacheable(value = "qb9.domain.RandChoiceReward")
    Optional<RandChoiceReward> findOneByGoal(Goal goal);

}
