package qb9.repository;

import qb9.domain.GiftFlip;
import qb9.domain.Goal;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the GiftFlip entity.
 */
@SuppressWarnings("unused")
public interface GiftFlipRepository extends JpaRepository<GiftFlip,Long> {

	
    @Cacheable(value = "qb9.domain.GiftFlip")	
    Optional<GiftFlip> findOneByGoal(Goal goal);

}
