package qb9;

import qb9.config.Constants;
import qb9.domain.Achievement;
import qb9.domain.FlipReward;
import qb9.domain.GiftFlip;
import qb9.domain.GiftItem;
import qb9.domain.Goal;
import qb9.domain.RandChoiceReward;
import qb9.domain.RandChoiceRewardOption;
import qb9.domain.PlayerAccount;
import qb9.domain.enumeration.GiftStatus;
import qb9.domain.enumeration.FlipStatus;
import qb9.domain.enumeration.RewardStrategyStatus;

import qb9.repository.AchievementRepository;
import qb9.repository.FlipRewardRepository;
import qb9.repository.GiftFlipRepository;
import qb9.repository.GiftItemRepository;
import qb9.repository.RandChoiceRewardRepository;
import qb9.repository.RandChoiceRewardOptionRepository;
import qb9.repository.GoalRepository;
import qb9.repository.PlayerAccountRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import javax.annotation.PostConstruct;
import javax.annotation.PostConstruct;
import javax.inject.Inject;



@Configuration
@Profile("dev")
public class DataLoader {


    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_CODE = "AAAAA";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String DEFAULT_SUMMARY = "AAAAA";
    private static final String DEFAULT_UID = "AAAAA";
    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String UPDATED_SUMMARY = "BBBBB";
    private static final String UPDATED_UID = "BBBBB";
    private static final String UPDATED_USERNAME = "BBBBB";


    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now();
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);


    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_SAFE_TITLE = "AAAAA";
    private static final String UPDATED_SAFE_TITLE = "BBBBB";

    private static final BigDecimal DEFAULT_DECIMAL = BigDecimal.valueOf(1.0);
    private static final BigDecimal UPDATED_DECIMAL = BigDecimal.valueOf(2.0);


    private static final Integer DEFAULT_NUMBER = 1;
    private static final Integer UPDATED_NUMBER = 2;
    private static final String DEFAULT_IMAGE_SRC = "AAAAA";
    private static final String UPDATED_IMAGE_SRC = "BBBBB";


    private final Logger log = LoggerFactory.getLogger(DataLoader.class);

    @Inject
    private Environment env;

    @Inject
    private PlayerAccountRepository accountRepository;

    @Inject
    private AchievementRepository achievementRepository;

    @Inject
    private GoalRepository goalRepository;

    @Inject
    private GiftItemRepository giftRepository;

    @Inject
    private GiftFlipRepository flipRepository;

    @Inject
    private FlipRewardRepository flipItemRepository;

    @Inject
    private RandChoiceRewardRepository randChoiceRewardRepository;

    @Inject
    private RandChoiceRewardOptionRepository randChoiceRewardOptionRepository;


    @PostConstruct
    public void loadData() {
        log.debug("Loading sample data v1");
        log.debug("now() => {}", ZonedDateTime.now());
        achievementRepository.deleteAll();
        flipItemRepository.deleteAll();
        flipRepository.deleteAll();
        randChoiceRewardOptionRepository.deleteAll();
        randChoiceRewardRepository.deleteAll();
        goalRepository.deleteAll();
        giftRepository.deleteAll();
        accountRepository.deleteAll();

        Goal endBallons = fakeGoal()
            .setCode("ENDBALLONS")
            .setDescription("End of Minigame Ballons");
        goalRepository.save(endBallons);
        Goal giftPinGoal = fakeGoal()
            .setCode("GIFTPIN")
            .setDescription("Gift Pin redeemed");
        goalRepository.save(giftPinGoal);
        goalRepository.save(fakeGoal());


        GiftItem gatoons01 = fakeGift().setCode("gatoonsEstatuas.estatua01").setDescription("Estatua Gatoons 01");
        giftRepository.save(gatoons01);
        GiftItem gatoons02 = fakeGift().setCode("gatoonsEstatuas.estatua02").setDescription("Estatua Gatoons 02");
        giftRepository.save(gatoons02);
        GiftItem gatoons03 = fakeGift().setCode("gatoonsEstatuas.estatua03").setDescription("Estatua Gatoons 03");
        giftRepository.save(gatoons03);
        GiftItem gatoons04 = fakeGift().setCode("gatoonsEstatuas.estatua04").setDescription("Estatua Gatoons 04");
        giftRepository.save(gatoons04);
        GiftItem gatoons05 = fakeGift().setCode("gatoonsEstatuas.estatua05").setDescription("Estatua Gatoons 05");
        giftRepository.save(gatoons05);

        GiftItem atadoPlumas = fakeGift()
            .setCode("pocketCrafting1.atadoPlumas")
            .setDescription("Atado de plumas");
        giftRepository.save(atadoPlumas);
        GiftItem bolaNieve = fakeGift()
            .setCode("pocketCrafting1.bolaNieve")
            .setDescription("Bola de nieve");
        giftRepository.save(bolaNieve);
        giftRepository.save(fakeGift());

        GiftFlip flip1 = fakeFlip()
            .setGoal(endBallons);
        flipRepository.save(flip1);
        FlipReward flip1Item1 = fakeFlipItem()
            .setFlip(flip1)
            .setReward(atadoPlumas);
        flipItemRepository.save(flip1Item1);
        FlipReward flip1Item2 = fakeFlipItem()
            .setFlip(flip1)
            .setReward(bolaNieve);
        flipItemRepository.save(flip1Item2);

        RandChoiceReward randChoice1 = fakeRandChoiceReward()
            .setGoal(giftPinGoal);
        randChoiceRewardRepository.save(randChoice1);
        RandChoiceRewardOption randChoice1Option1 = fakeRandChoiceRewardOption().setReward(randChoice1).setGiftItem(gatoons01).setP(BigDecimal.valueOf(0.2));
        randChoiceRewardOptionRepository.save(randChoice1Option1);
        RandChoiceRewardOption randChoice1Option2 = fakeRandChoiceRewardOption().setReward(randChoice1).setGiftItem(gatoons02).setP(BigDecimal.valueOf(0.2));
        randChoiceRewardOptionRepository.save(randChoice1Option2);
        RandChoiceRewardOption randChoice1Option3 = fakeRandChoiceRewardOption().setReward(randChoice1).setGiftItem(gatoons03).setP(BigDecimal.valueOf(0.2));
        randChoiceRewardOptionRepository.save(randChoice1Option3);
        RandChoiceRewardOption randChoice1Option4 = fakeRandChoiceRewardOption().setReward(randChoice1).setGiftItem(gatoons04).setP(BigDecimal.valueOf(0.2));
        randChoiceRewardOptionRepository.save(randChoice1Option4);
        RandChoiceRewardOption randChoice1Option5 = fakeRandChoiceRewardOption().setReward(randChoice1).setGiftItem(gatoons05).setP(BigDecimal.valueOf(0.2));
        randChoiceRewardOptionRepository.save(randChoice1Option5);

        log.debug("Sample data loaded");
    }

    private Goal fakeGoal() {
        UUID rand = UUID.randomUUID();
        String code = "reward.goal." + rand.toString().substring(
                rand.toString().lastIndexOf("-")+1);
        return new Goal()
            .setUid(rand.toString())
            .setCode(code)
            .setDescription("Description of "+code)
            .setCreatedAt(DEFAULT_CREATED_AT)
            .setUpdatedAt(DEFAULT_UPDATED_AT)
            .setMinRatio(DEFAULT_DECIMAL);
    }

    private GiftItem fakeGift() {
        UUID rand = UUID.randomUUID();
        String code = "reward.giftItem." + rand.toString().substring(
                rand.toString().lastIndexOf("-")+1);
        return new GiftItem()
            .setUid(rand.toString())
            .setCode(code)
            .setDescription("Description of " + code)
            .setCreatedAt(DEFAULT_CREATED_AT)
            .setUpdatedAt(DEFAULT_UPDATED_AT)
            .setStatus(GiftStatus.ACTIVE)
            .setUnitPack(1)
            .setImageSrc("https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150&txt="+code);

    }

    private GiftFlip fakeFlip() {
        UUID rand = UUID.randomUUID();
        String code = "reward.flip." + rand.toString().substring(
                rand.toString().lastIndexOf("-")+1);

         return new GiftFlip()
             .setCode(code)
             .setSummary("Summary of "+code)
             .setStatus(FlipStatus.ACTIVE)
             .setCreatedAt(DEFAULT_CREATED_AT)
             .setUpdatedAt(DEFAULT_UPDATED_AT)
             .setTurnRatio(BigDecimal.valueOf(150));
    }

    private FlipReward fakeFlipItem() {
         UUID rand = UUID.randomUUID();
         return new FlipReward()
             .setCreatedAt(DEFAULT_CREATED_AT)
             .setUpdatedAt(DEFAULT_UPDATED_AT)
             .setQty(Integer.valueOf(1))
             .setP(BigDecimal.valueOf(0.50));
    }

    private RandChoiceReward fakeRandChoiceReward() {
        UUID rand = UUID.randomUUID();
        String code = "reward.randChoiceReward." + rand.toString().substring(
                rand.toString().lastIndexOf("-")+1);

         return new RandChoiceReward()
             .setUid(rand.toString())
             .setCode(code)
             .setSummary("Summary of "+code)
             .setStatus(RewardStrategyStatus.ACTIVE)
             .setCreatedAt(DEFAULT_CREATED_AT)
             .setUpdatedAt(DEFAULT_UPDATED_AT);
    }

    private RandChoiceRewardOption fakeRandChoiceRewardOption() {
         return new RandChoiceRewardOption()
             .setCreatedAt(DEFAULT_CREATED_AT)
             .setUpdatedAt(DEFAULT_UPDATED_AT)
             .setQty(Integer.valueOf(1))
             .setP(BigDecimal.valueOf(0.50));
    }


//    private Account fakeAccount() {
//        Account account = new Account();
//        account.setUid("giftpin.account."+UUID.randomUUID())
//            .setEmail(DEFAULT_EMAIL)
//            .setUsername(DEFAULT_USERNAME)
//            .setCreatedAt(DEFAULT_CREATED_AT)
//            .setUpdatedAt(DEFAULT_UPDATED_AT)
//            .setStatus(AccountStatus.NEW);
//        return account;
//    }
//
//    private Account fakeAccount(String username) {
//        return fakeAccount()
//            .setUsername(username)
//            .setEmail(username+"@mail.com");
//    }


}
