/************************************************
 * Flip.java
 *
 * Each experiment consists of flipping n fair coins
 * with p success probability.
 ************************************************/
package qb9.math.std;

import qb9.math.std.StdRandom;



public final class StdFlip {

    private StdFlip() {}

    public static int flip(int n, double p) {
        int heads = 0;
        for (int j = 0; j < n; j++) {
            if (StdRandom.bernoulli(p)) {
                heads++;
            }
        }
        return heads;
    }

}

