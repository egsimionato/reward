/********************************************
 * StdRandom.java
 *
 * A library of static methods to generate pseudo-random numbers from
 * a bernoulli distribution.
 *
 * Remark
 * -------
 *  - Reliees on randomness of nextDouble() method in java.util.Random
 *    to generate pseudorandom numbers in [0, 1).
 *
 ********************************************/
package qb9.math.std;

import java.util.Random;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.apache.commons.math3.distribution.EnumeratedDistribution;

/**
 * The {@code StdRandom} class provides static methods for generating
 * random number from Bernoulli distribution.
 *
 **/
public final class StdRandom {

    private static Random random;

    static {
         random = new Random();
    }

    private StdRandom() {}

    /**
     * Return a random integer uniformly in [0, 1).
     *
     * @return a random real number uniformly in [0, 1)
     */
    public static double uniform() {
        return random.nextDouble();
    }

    /**
     * Return a random boolean from a Bernoulli distribution with success
     * probability <em>p</em>.
     *
     * @param p the probability of returning <tt>true</tt>
     * @return <tt>true</tt> with probability <tt>p</tt> and
     *          <tt>false</tt> with probability <tt>p</tt>
     * @throws IllegalArgumentException unless <tt>p >= 0.0</tt> and <tt>p <= 1.0</tt>
     */
    public static boolean bernoulli(double p) {
        if (!(p > 0.0 && p <= 1.0)) {
            throw new IllegalArgumentException("Probability must be between 0.0 and 1.0");
        }
        return uniform() < p;
    }

    public static <T> T randomChoice(List<Pair<T, Double>> pmf) {
        EnumeratedDistribution<T> distribution = new EnumeratedDistribution<T>(pmf);
        return distribution.sample();
    }

}
