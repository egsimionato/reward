package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.Achievement;
import qb9.repository.AchievementRepository;
import qb9.service.AchievementService;
import qb9.web.rest.dto.AchievementDTO;
import qb9.web.rest.mapper.AchievementMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.AchievementStatus;

/**
 * Test class for the AchievementResource REST controller.
 *
 * @see AchievementResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RewardApp.class)
@WebAppConfiguration
@IntegrationTest
public class AchievementResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_PROCESSED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_PROCESSED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_PROCESSED_AT_STR = dateTimeFormatter.format(DEFAULT_PROCESSED_AT);

    private static final AchievementStatus DEFAULT_STATUS = AchievementStatus.NEW;
    private static final AchievementStatus UPDATED_STATUS = AchievementStatus.PROCESSED;

    private static final BigDecimal DEFAULT_RATIO = new BigDecimal(1);
    private static final BigDecimal UPDATED_RATIO = new BigDecimal(2);

    @Inject
    private AchievementRepository achievementRepository;

    @Inject
    private AchievementMapper achievementMapper;

    @Inject
    private AchievementService achievementService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restAchievementMockMvc;

    private Achievement achievement;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AchievementResource achievementResource = new AchievementResource();
        ReflectionTestUtils.setField(achievementResource, "achievementService", achievementService);
        ReflectionTestUtils.setField(achievementResource, "achievementMapper", achievementMapper);
        this.restAchievementMockMvc = MockMvcBuilders.standaloneSetup(achievementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        achievement = new Achievement();
        achievement.setCreatedAt(DEFAULT_CREATED_AT);
        achievement.setProcessedAt(DEFAULT_PROCESSED_AT);
        achievement.setStatus(DEFAULT_STATUS);
        achievement.setRatio(DEFAULT_RATIO);
    }

    @Test
    @Transactional
    public void createAchievement() throws Exception {
        int databaseSizeBeforeCreate = achievementRepository.findAll().size();

        // Create the Achievement
        AchievementDTO achievementDTO = achievementMapper.achievementToAchievementDTO(achievement);

        restAchievementMockMvc.perform(post("/api/achievements")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(achievementDTO)))
                .andExpect(status().isCreated());

        // Validate the Achievement in the database
        List<Achievement> achievements = achievementRepository.findAll();
        assertThat(achievements).hasSize(databaseSizeBeforeCreate + 1);
        Achievement testAchievement = achievements.get(achievements.size() - 1);
        assertThat(testAchievement.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAchievement.getProcessedAt()).isEqualTo(DEFAULT_PROCESSED_AT);
        assertThat(testAchievement.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAchievement.getRatio()).isEqualTo(DEFAULT_RATIO);
    }

    @Test
    @Transactional
    public void getAllAchievements() throws Exception {
        // Initialize the database
        achievementRepository.saveAndFlush(achievement);

        // Get all the achievements
        restAchievementMockMvc.perform(get("/api/achievements?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(achievement.getId().intValue())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].processedAt").value(hasItem(DEFAULT_PROCESSED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].ratio").value(hasItem(DEFAULT_RATIO.intValue())));
    }

    @Test
    @Transactional
    public void getAchievement() throws Exception {
        // Initialize the database
        achievementRepository.saveAndFlush(achievement);

        // Get the achievement
        restAchievementMockMvc.perform(get("/api/achievements/{id}", achievement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(achievement.getId().intValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.processedAt").value(DEFAULT_PROCESSED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.ratio").value(DEFAULT_RATIO.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAchievement() throws Exception {
        // Get the achievement
        restAchievementMockMvc.perform(get("/api/achievements/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAchievement() throws Exception {
        // Initialize the database
        achievementRepository.saveAndFlush(achievement);
        int databaseSizeBeforeUpdate = achievementRepository.findAll().size();

        // Update the achievement
        Achievement updatedAchievement = new Achievement();
        updatedAchievement.setId(achievement.getId());
        updatedAchievement.setCreatedAt(UPDATED_CREATED_AT);
        updatedAchievement.setProcessedAt(UPDATED_PROCESSED_AT);
        updatedAchievement.setStatus(UPDATED_STATUS);
        updatedAchievement.setRatio(UPDATED_RATIO);
        AchievementDTO achievementDTO = achievementMapper.achievementToAchievementDTO(updatedAchievement);

        restAchievementMockMvc.perform(put("/api/achievements")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(achievementDTO)))
                .andExpect(status().isOk());

        // Validate the Achievement in the database
        List<Achievement> achievements = achievementRepository.findAll();
        assertThat(achievements).hasSize(databaseSizeBeforeUpdate);
        Achievement testAchievement = achievements.get(achievements.size() - 1);
        assertThat(testAchievement.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testAchievement.getProcessedAt()).isEqualTo(UPDATED_PROCESSED_AT);
        assertThat(testAchievement.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAchievement.getRatio()).isEqualTo(UPDATED_RATIO);
    }

    @Test
    @Transactional
    public void deleteAchievement() throws Exception {
        // Initialize the database
        achievementRepository.saveAndFlush(achievement);
        int databaseSizeBeforeDelete = achievementRepository.findAll().size();

        // Get the achievement
        restAchievementMockMvc.perform(delete("/api/achievements/{id}", achievement.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Achievement> achievements = achievementRepository.findAll();
        assertThat(achievements).hasSize(databaseSizeBeforeDelete - 1);
    }
}
