package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.GiftFlip;
import qb9.repository.GiftFlipRepository;
import qb9.service.GiftFlipService;
import qb9.web.rest.dto.GiftFlipDTO;
import qb9.web.rest.mapper.GiftFlipMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.FlipStatus;

/**
 * Test class for the GiftFlipResource REST controller.
 *
 * @see GiftFlipResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RewardApp.class)
@WebAppConfiguration
@IntegrationTest
public class GiftFlipResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_CODE = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String DEFAULT_SUMMARY = "AAAAA";
    private static final String UPDATED_SUMMARY = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final FlipStatus DEFAULT_STATUS = FlipStatus.DRAFT;
    private static final FlipStatus UPDATED_STATUS = FlipStatus.ACTIVE;

    private static final BigDecimal DEFAULT_TURN_RATIO = new BigDecimal(1);
    private static final BigDecimal UPDATED_TURN_RATIO = new BigDecimal(2);

    @Inject
    private GiftFlipRepository giftFlipRepository;

    @Inject
    private GiftFlipMapper giftFlipMapper;

    @Inject
    private GiftFlipService giftFlipService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGiftFlipMockMvc;

    private GiftFlip giftFlip;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GiftFlipResource giftFlipResource = new GiftFlipResource();
        ReflectionTestUtils.setField(giftFlipResource, "giftFlipService", giftFlipService);
        ReflectionTestUtils.setField(giftFlipResource, "giftFlipMapper", giftFlipMapper);
        this.restGiftFlipMockMvc = MockMvcBuilders.standaloneSetup(giftFlipResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        giftFlip = new GiftFlip();
        giftFlip.setCode(DEFAULT_CODE);
        giftFlip.setSummary(DEFAULT_SUMMARY);
        giftFlip.setCreatedAt(DEFAULT_CREATED_AT);
        giftFlip.setUpdatedAt(DEFAULT_UPDATED_AT);
        giftFlip.setStatus(DEFAULT_STATUS);
        giftFlip.setTurnRatio(DEFAULT_TURN_RATIO);
    }

    @Test
    @Transactional
    public void createGiftFlip() throws Exception {
        int databaseSizeBeforeCreate = giftFlipRepository.findAll().size();

        // Create the GiftFlip
        GiftFlipDTO giftFlipDTO = giftFlipMapper.giftFlipToGiftFlipDTO(giftFlip);

        restGiftFlipMockMvc.perform(post("/api/gift-flips")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftFlipDTO)))
                .andExpect(status().isCreated());

        // Validate the GiftFlip in the database
        List<GiftFlip> giftFlips = giftFlipRepository.findAll();
        assertThat(giftFlips).hasSize(databaseSizeBeforeCreate + 1);
        GiftFlip testGiftFlip = giftFlips.get(giftFlips.size() - 1);
        assertThat(testGiftFlip.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testGiftFlip.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testGiftFlip.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testGiftFlip.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testGiftFlip.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testGiftFlip.getTurnRatio()).isEqualTo(DEFAULT_TURN_RATIO);
    }

    @Test
    @Transactional
    public void getAllGiftFlips() throws Exception {
        // Initialize the database
        giftFlipRepository.saveAndFlush(giftFlip);

        // Get all the giftFlips
        restGiftFlipMockMvc.perform(get("/api/gift-flips?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(giftFlip.getId().intValue())))
                .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
                .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].turnRatio").value(hasItem(DEFAULT_TURN_RATIO.intValue())));
    }

    @Test
    @Transactional
    public void getGiftFlip() throws Exception {
        // Initialize the database
        giftFlipRepository.saveAndFlush(giftFlip);

        // Get the giftFlip
        restGiftFlipMockMvc.perform(get("/api/gift-flips/{id}", giftFlip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(giftFlip.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.summary").value(DEFAULT_SUMMARY.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.turnRatio").value(DEFAULT_TURN_RATIO.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingGiftFlip() throws Exception {
        // Get the giftFlip
        restGiftFlipMockMvc.perform(get("/api/gift-flips/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGiftFlip() throws Exception {
        // Initialize the database
        giftFlipRepository.saveAndFlush(giftFlip);
        int databaseSizeBeforeUpdate = giftFlipRepository.findAll().size();

        // Update the giftFlip
        GiftFlip updatedGiftFlip = new GiftFlip();
        updatedGiftFlip.setId(giftFlip.getId());
        updatedGiftFlip.setCode(UPDATED_CODE);
        updatedGiftFlip.setSummary(UPDATED_SUMMARY);
        updatedGiftFlip.setCreatedAt(UPDATED_CREATED_AT);
        updatedGiftFlip.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedGiftFlip.setStatus(UPDATED_STATUS);
        updatedGiftFlip.setTurnRatio(UPDATED_TURN_RATIO);
        GiftFlipDTO giftFlipDTO = giftFlipMapper.giftFlipToGiftFlipDTO(updatedGiftFlip);

        restGiftFlipMockMvc.perform(put("/api/gift-flips")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftFlipDTO)))
                .andExpect(status().isOk());

        // Validate the GiftFlip in the database
        List<GiftFlip> giftFlips = giftFlipRepository.findAll();
        assertThat(giftFlips).hasSize(databaseSizeBeforeUpdate);
        GiftFlip testGiftFlip = giftFlips.get(giftFlips.size() - 1);
        assertThat(testGiftFlip.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testGiftFlip.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testGiftFlip.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testGiftFlip.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testGiftFlip.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testGiftFlip.getTurnRatio()).isEqualTo(UPDATED_TURN_RATIO);
    }

    @Test
    @Transactional
    public void deleteGiftFlip() throws Exception {
        // Initialize the database
        giftFlipRepository.saveAndFlush(giftFlip);
        int databaseSizeBeforeDelete = giftFlipRepository.findAll().size();

        // Get the giftFlip
        restGiftFlipMockMvc.perform(delete("/api/gift-flips/{id}", giftFlip.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<GiftFlip> giftFlips = giftFlipRepository.findAll();
        assertThat(giftFlips).hasSize(databaseSizeBeforeDelete - 1);
    }
}
