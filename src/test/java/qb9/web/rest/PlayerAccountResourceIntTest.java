package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.PlayerAccount;
import qb9.repository.PlayerAccountRepository;
import qb9.service.PlayerAccountService;
import qb9.web.rest.dto.PlayerAccountDTO;
import qb9.web.rest.mapper.PlayerAccountMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.AccountStatus;

/**
 * Test class for the PlayerAccountResource REST controller.
 *
 * @see PlayerAccountResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RewardApp.class)
@WebAppConfiguration
@IntegrationTest
public class PlayerAccountResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_USERNAME = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final AccountStatus DEFAULT_STATUS = AccountStatus.NEW;
    private static final AccountStatus UPDATED_STATUS = AccountStatus.ACTIVE;

    @Inject
    private PlayerAccountRepository playerAccountRepository;

    @Inject
    private PlayerAccountMapper playerAccountMapper;

    @Inject
    private PlayerAccountService playerAccountService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPlayerAccountMockMvc;

    private PlayerAccount playerAccount;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlayerAccountResource playerAccountResource = new PlayerAccountResource();
        ReflectionTestUtils.setField(playerAccountResource, "playerAccountService", playerAccountService);
        ReflectionTestUtils.setField(playerAccountResource, "playerAccountMapper", playerAccountMapper);
        this.restPlayerAccountMockMvc = MockMvcBuilders.standaloneSetup(playerAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        playerAccount = new PlayerAccount();
        playerAccount.setUid(DEFAULT_UID);
        playerAccount.setEmail(DEFAULT_EMAIL);
        playerAccount.setUsername(DEFAULT_USERNAME);
        playerAccount.setCreatedAt(DEFAULT_CREATED_AT);
        playerAccount.setUpdatedAt(DEFAULT_UPDATED_AT);
        playerAccount.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createPlayerAccount() throws Exception {
        int databaseSizeBeforeCreate = playerAccountRepository.findAll().size();

        // Create the PlayerAccount
        PlayerAccountDTO playerAccountDTO = playerAccountMapper.playerAccountToPlayerAccountDTO(playerAccount);

        restPlayerAccountMockMvc.perform(post("/api/player-accounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playerAccountDTO)))
                .andExpect(status().isCreated());

        // Validate the PlayerAccount in the database
        List<PlayerAccount> playerAccounts = playerAccountRepository.findAll();
        assertThat(playerAccounts).hasSize(databaseSizeBeforeCreate + 1);
        PlayerAccount testPlayerAccount = playerAccounts.get(playerAccounts.size() - 1);
        assertThat(testPlayerAccount.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testPlayerAccount.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPlayerAccount.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testPlayerAccount.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPlayerAccount.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPlayerAccount.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllPlayerAccounts() throws Exception {
        // Initialize the database
        playerAccountRepository.saveAndFlush(playerAccount);

        // Get all the playerAccounts
        restPlayerAccountMockMvc.perform(get("/api/player-accounts?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(playerAccount.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getPlayerAccount() throws Exception {
        // Initialize the database
        playerAccountRepository.saveAndFlush(playerAccount);

        // Get the playerAccount
        restPlayerAccountMockMvc.perform(get("/api/player-accounts/{id}", playerAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(playerAccount.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlayerAccount() throws Exception {
        // Get the playerAccount
        restPlayerAccountMockMvc.perform(get("/api/player-accounts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlayerAccount() throws Exception {
        // Initialize the database
        playerAccountRepository.saveAndFlush(playerAccount);
        int databaseSizeBeforeUpdate = playerAccountRepository.findAll().size();

        // Update the playerAccount
        PlayerAccount updatedPlayerAccount = new PlayerAccount();
        updatedPlayerAccount.setId(playerAccount.getId());
        updatedPlayerAccount.setUid(UPDATED_UID);
        updatedPlayerAccount.setEmail(UPDATED_EMAIL);
        updatedPlayerAccount.setUsername(UPDATED_USERNAME);
        updatedPlayerAccount.setCreatedAt(UPDATED_CREATED_AT);
        updatedPlayerAccount.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedPlayerAccount.setStatus(UPDATED_STATUS);
        PlayerAccountDTO playerAccountDTO = playerAccountMapper.playerAccountToPlayerAccountDTO(updatedPlayerAccount);

        restPlayerAccountMockMvc.perform(put("/api/player-accounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playerAccountDTO)))
                .andExpect(status().isOk());

        // Validate the PlayerAccount in the database
        List<PlayerAccount> playerAccounts = playerAccountRepository.findAll();
        assertThat(playerAccounts).hasSize(databaseSizeBeforeUpdate);
        PlayerAccount testPlayerAccount = playerAccounts.get(playerAccounts.size() - 1);
        assertThat(testPlayerAccount.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testPlayerAccount.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPlayerAccount.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testPlayerAccount.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPlayerAccount.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPlayerAccount.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deletePlayerAccount() throws Exception {
        // Initialize the database
        playerAccountRepository.saveAndFlush(playerAccount);
        int databaseSizeBeforeDelete = playerAccountRepository.findAll().size();

        // Get the playerAccount
        restPlayerAccountMockMvc.perform(delete("/api/player-accounts/{id}", playerAccount.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PlayerAccount> playerAccounts = playerAccountRepository.findAll();
        assertThat(playerAccounts).hasSize(databaseSizeBeforeDelete - 1);
    }
}
