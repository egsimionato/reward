package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.GiftItem;
import qb9.repository.GiftItemRepository;
import qb9.service.GiftItemService;
import qb9.web.rest.dto.GiftItemDTO;
import qb9.web.rest.mapper.GiftItemMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.GiftStatus;

/**
 * Test class for the GiftItemResource REST controller.
 *
 * @see GiftItemResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RewardApp.class)
@WebAppConfiguration
@IntegrationTest
public class GiftItemResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_CODE = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final GiftStatus DEFAULT_STATUS = GiftStatus.ACTIVE;
    private static final GiftStatus UPDATED_STATUS = GiftStatus.DISABLED;

    private static final Integer DEFAULT_UNIT_PACK = 1;
    private static final Integer UPDATED_UNIT_PACK = 2;
    private static final String DEFAULT_IMAGE_SRC = "AAAAA";
    private static final String UPDATED_IMAGE_SRC = "BBBBB";

    @Inject
    private GiftItemRepository giftItemRepository;

    @Inject
    private GiftItemMapper giftItemMapper;

    @Inject
    private GiftItemService giftItemService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGiftItemMockMvc;

    private GiftItem giftItem;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GiftItemResource giftItemResource = new GiftItemResource();
        ReflectionTestUtils.setField(giftItemResource, "giftItemService", giftItemService);
        ReflectionTestUtils.setField(giftItemResource, "giftItemMapper", giftItemMapper);
        this.restGiftItemMockMvc = MockMvcBuilders.standaloneSetup(giftItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        giftItem = new GiftItem();
        giftItem.setUid(DEFAULT_UID);
        giftItem.setCode(DEFAULT_CODE);
        giftItem.setDescription(DEFAULT_DESCRIPTION);
        giftItem.setCreatedAt(DEFAULT_CREATED_AT);
        giftItem.setUpdatedAt(DEFAULT_UPDATED_AT);
        giftItem.setStatus(DEFAULT_STATUS);
        giftItem.setUnitPack(DEFAULT_UNIT_PACK);
        giftItem.setImageSrc(DEFAULT_IMAGE_SRC);
    }

    @Test
    @Transactional
    public void createGiftItem() throws Exception {
        int databaseSizeBeforeCreate = giftItemRepository.findAll().size();

        // Create the GiftItem
        GiftItemDTO giftItemDTO = giftItemMapper.giftItemToGiftItemDTO(giftItem);

        restGiftItemMockMvc.perform(post("/api/gift-items")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftItemDTO)))
                .andExpect(status().isCreated());

        // Validate the GiftItem in the database
        List<GiftItem> giftItems = giftItemRepository.findAll();
        assertThat(giftItems).hasSize(databaseSizeBeforeCreate + 1);
        GiftItem testGiftItem = giftItems.get(giftItems.size() - 1);
        assertThat(testGiftItem.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testGiftItem.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testGiftItem.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testGiftItem.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testGiftItem.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testGiftItem.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testGiftItem.getUnitPack()).isEqualTo(DEFAULT_UNIT_PACK);
        assertThat(testGiftItem.getImageSrc()).isEqualTo(DEFAULT_IMAGE_SRC);
    }

    @Test
    @Transactional
    public void getAllGiftItems() throws Exception {
        // Initialize the database
        giftItemRepository.saveAndFlush(giftItem);

        // Get all the giftItems
        restGiftItemMockMvc.perform(get("/api/gift-items?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(giftItem.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].unitPack").value(hasItem(DEFAULT_UNIT_PACK)))
                .andExpect(jsonPath("$.[*].imageSrc").value(hasItem(DEFAULT_IMAGE_SRC.toString())));
    }

    @Test
    @Transactional
    public void getGiftItem() throws Exception {
        // Initialize the database
        giftItemRepository.saveAndFlush(giftItem);

        // Get the giftItem
        restGiftItemMockMvc.perform(get("/api/gift-items/{id}", giftItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(giftItem.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.unitPack").value(DEFAULT_UNIT_PACK))
            .andExpect(jsonPath("$.imageSrc").value(DEFAULT_IMAGE_SRC.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGiftItem() throws Exception {
        // Get the giftItem
        restGiftItemMockMvc.perform(get("/api/gift-items/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGiftItem() throws Exception {
        // Initialize the database
        giftItemRepository.saveAndFlush(giftItem);
        int databaseSizeBeforeUpdate = giftItemRepository.findAll().size();

        // Update the giftItem
        GiftItem updatedGiftItem = new GiftItem();
        updatedGiftItem.setId(giftItem.getId());
        updatedGiftItem.setUid(UPDATED_UID);
        updatedGiftItem.setCode(UPDATED_CODE);
        updatedGiftItem.setDescription(UPDATED_DESCRIPTION);
        updatedGiftItem.setCreatedAt(UPDATED_CREATED_AT);
        updatedGiftItem.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedGiftItem.setStatus(UPDATED_STATUS);
        updatedGiftItem.setUnitPack(UPDATED_UNIT_PACK);
        updatedGiftItem.setImageSrc(UPDATED_IMAGE_SRC);
        GiftItemDTO giftItemDTO = giftItemMapper.giftItemToGiftItemDTO(updatedGiftItem);

        restGiftItemMockMvc.perform(put("/api/gift-items")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftItemDTO)))
                .andExpect(status().isOk());

        // Validate the GiftItem in the database
        List<GiftItem> giftItems = giftItemRepository.findAll();
        assertThat(giftItems).hasSize(databaseSizeBeforeUpdate);
        GiftItem testGiftItem = giftItems.get(giftItems.size() - 1);
        assertThat(testGiftItem.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testGiftItem.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testGiftItem.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testGiftItem.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testGiftItem.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testGiftItem.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testGiftItem.getUnitPack()).isEqualTo(UPDATED_UNIT_PACK);
        assertThat(testGiftItem.getImageSrc()).isEqualTo(UPDATED_IMAGE_SRC);
    }

    @Test
    @Transactional
    public void deleteGiftItem() throws Exception {
        // Initialize the database
        giftItemRepository.saveAndFlush(giftItem);
        int databaseSizeBeforeDelete = giftItemRepository.findAll().size();

        // Get the giftItem
        restGiftItemMockMvc.perform(delete("/api/gift-items/{id}", giftItem.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<GiftItem> giftItems = giftItemRepository.findAll();
        assertThat(giftItems).hasSize(databaseSizeBeforeDelete - 1);
    }
}
