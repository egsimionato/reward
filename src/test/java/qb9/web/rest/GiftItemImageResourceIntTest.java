package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.GiftItemImage;
import qb9.repository.GiftItemImageRepository;
import qb9.service.GiftItemImageService;
import qb9.web.rest.dto.GiftItemImageDTO;
import qb9.web.rest.mapper.GiftItemImageMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the GiftItemImageResource REST controller.
 *
 * @see GiftItemImageResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RewardApp.class)
@WebAppConfiguration
@IntegrationTest
public class GiftItemImageResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_UUID = "AAAAA";
    private static final String UPDATED_UUID = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);
    private static final String DEFAULT_SRC = "AAAAA";
    private static final String UPDATED_SRC = "BBBBB";

    @Inject
    private GiftItemImageRepository giftItemImageRepository;

    @Inject
    private GiftItemImageMapper giftItemImageMapper;

    @Inject
    private GiftItemImageService giftItemImageService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGiftItemImageMockMvc;

    private GiftItemImage giftItemImage;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GiftItemImageResource giftItemImageResource = new GiftItemImageResource();
        ReflectionTestUtils.setField(giftItemImageResource, "giftItemImageService", giftItemImageService);
        ReflectionTestUtils.setField(giftItemImageResource, "giftItemImageMapper", giftItemImageMapper);
        this.restGiftItemImageMockMvc = MockMvcBuilders.standaloneSetup(giftItemImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        giftItemImage = new GiftItemImage();
        giftItemImage.setUuid(DEFAULT_UUID);
        giftItemImage.setCreated_at(DEFAULT_CREATED_AT);
        giftItemImage.setUpdated_at(DEFAULT_UPDATED_AT);
        giftItemImage.setSrc(DEFAULT_SRC);
    }

    @Test
    @Transactional
    public void createGiftItemImage() throws Exception {
        int databaseSizeBeforeCreate = giftItemImageRepository.findAll().size();

        // Create the GiftItemImage
        GiftItemImageDTO giftItemImageDTO = giftItemImageMapper.giftItemImageToGiftItemImageDTO(giftItemImage);

        restGiftItemImageMockMvc.perform(post("/api/gift-item-images")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftItemImageDTO)))
                .andExpect(status().isCreated());

        // Validate the GiftItemImage in the database
        List<GiftItemImage> giftItemImages = giftItemImageRepository.findAll();
        assertThat(giftItemImages).hasSize(databaseSizeBeforeCreate + 1);
        GiftItemImage testGiftItemImage = giftItemImages.get(giftItemImages.size() - 1);
        assertThat(testGiftItemImage.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testGiftItemImage.getCreated_at()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testGiftItemImage.getUpdated_at()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testGiftItemImage.getSrc()).isEqualTo(DEFAULT_SRC);
    }

    @Test
    @Transactional
    public void getAllGiftItemImages() throws Exception {
        // Initialize the database
        giftItemImageRepository.saveAndFlush(giftItemImage);

        // Get all the giftItemImages
        restGiftItemImageMockMvc.perform(get("/api/gift-item-images?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(giftItemImage.getId().intValue())))
                .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
                .andExpect(jsonPath("$.[*].created_at").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updated_at").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].src").value(hasItem(DEFAULT_SRC.toString())));
    }

    @Test
    @Transactional
    public void getGiftItemImage() throws Exception {
        // Initialize the database
        giftItemImageRepository.saveAndFlush(giftItemImage);

        // Get the giftItemImage
        restGiftItemImageMockMvc.perform(get("/api/gift-item-images/{id}", giftItemImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(giftItemImage.getId().intValue()))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID.toString()))
            .andExpect(jsonPath("$.created_at").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updated_at").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.src").value(DEFAULT_SRC.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGiftItemImage() throws Exception {
        // Get the giftItemImage
        restGiftItemImageMockMvc.perform(get("/api/gift-item-images/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGiftItemImage() throws Exception {
        // Initialize the database
        giftItemImageRepository.saveAndFlush(giftItemImage);
        int databaseSizeBeforeUpdate = giftItemImageRepository.findAll().size();

        // Update the giftItemImage
        GiftItemImage updatedGiftItemImage = new GiftItemImage();
        updatedGiftItemImage.setId(giftItemImage.getId());
        updatedGiftItemImage.setUuid(UPDATED_UUID);
        updatedGiftItemImage.setCreated_at(UPDATED_CREATED_AT);
        updatedGiftItemImage.setUpdated_at(UPDATED_UPDATED_AT);
        updatedGiftItemImage.setSrc(UPDATED_SRC);
        GiftItemImageDTO giftItemImageDTO = giftItemImageMapper.giftItemImageToGiftItemImageDTO(updatedGiftItemImage);

        restGiftItemImageMockMvc.perform(put("/api/gift-item-images")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftItemImageDTO)))
                .andExpect(status().isOk());

        // Validate the GiftItemImage in the database
        List<GiftItemImage> giftItemImages = giftItemImageRepository.findAll();
        assertThat(giftItemImages).hasSize(databaseSizeBeforeUpdate);
        GiftItemImage testGiftItemImage = giftItemImages.get(giftItemImages.size() - 1);
        assertThat(testGiftItemImage.getUuid()).isEqualTo(UPDATED_UUID);
        assertThat(testGiftItemImage.getCreated_at()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testGiftItemImage.getUpdated_at()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testGiftItemImage.getSrc()).isEqualTo(UPDATED_SRC);
    }

    @Test
    @Transactional
    public void deleteGiftItemImage() throws Exception {
        // Initialize the database
        giftItemImageRepository.saveAndFlush(giftItemImage);
        int databaseSizeBeforeDelete = giftItemImageRepository.findAll().size();

        // Get the giftItemImage
        restGiftItemImageMockMvc.perform(delete("/api/gift-item-images/{id}", giftItemImage.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<GiftItemImage> giftItemImages = giftItemImageRepository.findAll();
        assertThat(giftItemImages).hasSize(databaseSizeBeforeDelete - 1);
    }
}
