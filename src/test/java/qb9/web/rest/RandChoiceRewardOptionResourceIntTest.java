package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.RandChoiceRewardOption;
import qb9.repository.RandChoiceRewardOptionRepository;
import qb9.service.RandChoiceRewardOptionService;
import qb9.service.dto.RandChoiceRewardOptionDTO;
import qb9.service.mapper.RandChoiceRewardOptionMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RandChoiceRewardOptionResource REST controller.
 *
 * @see RandChoiceRewardOptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RewardApp.class)
public class RandChoiceRewardOptionResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final BigDecimal DEFAULT_P = new BigDecimal(1);
    private static final BigDecimal UPDATED_P = new BigDecimal(2);

    private static final Integer DEFAULT_QTY = 1;
    private static final Integer UPDATED_QTY = 2;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    @Inject
    private RandChoiceRewardOptionRepository randChoiceRewardOptionRepository;

    @Inject
    private RandChoiceRewardOptionMapper randChoiceRewardOptionMapper;

    @Inject
    private RandChoiceRewardOptionService randChoiceRewardOptionService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restRandChoiceRewardOptionMockMvc;

    private RandChoiceRewardOption randChoiceRewardOption;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        RandChoiceRewardOptionResource randChoiceRewardOptionResource = new RandChoiceRewardOptionResource();
        ReflectionTestUtils.setField(randChoiceRewardOptionResource, "randChoiceRewardOptionService", randChoiceRewardOptionService);
        this.restRandChoiceRewardOptionMockMvc = MockMvcBuilders.standaloneSetup(randChoiceRewardOptionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RandChoiceRewardOption createEntity(EntityManager em) {
        RandChoiceRewardOption randChoiceRewardOption = new RandChoiceRewardOption();
        randChoiceRewardOption = new RandChoiceRewardOption();
        randChoiceRewardOption.setP(DEFAULT_P);
        randChoiceRewardOption.setQty(DEFAULT_QTY);
        randChoiceRewardOption.setCreatedAt(DEFAULT_CREATED_AT);
        randChoiceRewardOption.setUpdatedAt(DEFAULT_UPDATED_AT);
        return randChoiceRewardOption;
    }

    @Before
    public void initTest() {
        randChoiceRewardOption = createEntity(em);
    }

    @Test
    @Transactional
    public void createRandChoiceRewardOption() throws Exception {
        int databaseSizeBeforeCreate = randChoiceRewardOptionRepository.findAll().size();

        // Create the RandChoiceRewardOption
        RandChoiceRewardOptionDTO randChoiceRewardOptionDTO = randChoiceRewardOptionMapper.randChoiceRewardOptionToRandChoiceRewardOptionDTO(randChoiceRewardOption);

        restRandChoiceRewardOptionMockMvc.perform(post("/api/rand-choice-reward-options")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(randChoiceRewardOptionDTO)))
                .andExpect(status().isCreated());

        // Validate the RandChoiceRewardOption in the database
        List<RandChoiceRewardOption> randChoiceRewardOptions = randChoiceRewardOptionRepository.findAll();
        assertThat(randChoiceRewardOptions).hasSize(databaseSizeBeforeCreate + 1);
        RandChoiceRewardOption testRandChoiceRewardOption = randChoiceRewardOptions.get(randChoiceRewardOptions.size() - 1);
        assertThat(testRandChoiceRewardOption.getP()).isEqualTo(DEFAULT_P);
        assertThat(testRandChoiceRewardOption.getQty()).isEqualTo(DEFAULT_QTY);
        assertThat(testRandChoiceRewardOption.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testRandChoiceRewardOption.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    public void getAllRandChoiceRewardOptions() throws Exception {
        // Initialize the database
        randChoiceRewardOptionRepository.saveAndFlush(randChoiceRewardOption);

        // Get all the randChoiceRewardOptions
        restRandChoiceRewardOptionMockMvc.perform(get("/api/rand-choice-reward-options?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(randChoiceRewardOption.getId().intValue())))
                .andExpect(jsonPath("$.[*].p").value(hasItem(DEFAULT_P.intValue())))
                .andExpect(jsonPath("$.[*].qty").value(hasItem(DEFAULT_QTY)))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)));
    }

    @Test
    @Transactional
    public void getRandChoiceRewardOption() throws Exception {
        // Initialize the database
        randChoiceRewardOptionRepository.saveAndFlush(randChoiceRewardOption);

        // Get the randChoiceRewardOption
        restRandChoiceRewardOptionMockMvc.perform(get("/api/rand-choice-reward-options/{id}", randChoiceRewardOption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(randChoiceRewardOption.getId().intValue()))
            .andExpect(jsonPath("$.p").value(DEFAULT_P.intValue()))
            .andExpect(jsonPath("$.qty").value(DEFAULT_QTY))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR));
    }

    @Test
    @Transactional
    public void getNonExistingRandChoiceRewardOption() throws Exception {
        // Get the randChoiceRewardOption
        restRandChoiceRewardOptionMockMvc.perform(get("/api/rand-choice-reward-options/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRandChoiceRewardOption() throws Exception {
        // Initialize the database
        randChoiceRewardOptionRepository.saveAndFlush(randChoiceRewardOption);
        int databaseSizeBeforeUpdate = randChoiceRewardOptionRepository.findAll().size();

        // Update the randChoiceRewardOption
        RandChoiceRewardOption updatedRandChoiceRewardOption = randChoiceRewardOptionRepository.findOne(randChoiceRewardOption.getId());
        updatedRandChoiceRewardOption.setP(UPDATED_P);
        updatedRandChoiceRewardOption.setQty(UPDATED_QTY);
        updatedRandChoiceRewardOption.setCreatedAt(UPDATED_CREATED_AT);
        updatedRandChoiceRewardOption.setUpdatedAt(UPDATED_UPDATED_AT);
        RandChoiceRewardOptionDTO randChoiceRewardOptionDTO = randChoiceRewardOptionMapper.randChoiceRewardOptionToRandChoiceRewardOptionDTO(updatedRandChoiceRewardOption);

        restRandChoiceRewardOptionMockMvc.perform(put("/api/rand-choice-reward-options")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(randChoiceRewardOptionDTO)))
                .andExpect(status().isOk());

        // Validate the RandChoiceRewardOption in the database
        List<RandChoiceRewardOption> randChoiceRewardOptions = randChoiceRewardOptionRepository.findAll();
        assertThat(randChoiceRewardOptions).hasSize(databaseSizeBeforeUpdate);
        RandChoiceRewardOption testRandChoiceRewardOption = randChoiceRewardOptions.get(randChoiceRewardOptions.size() - 1);
        assertThat(testRandChoiceRewardOption.getP()).isEqualTo(UPDATED_P);
        assertThat(testRandChoiceRewardOption.getQty()).isEqualTo(UPDATED_QTY);
        assertThat(testRandChoiceRewardOption.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testRandChoiceRewardOption.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    public void deleteRandChoiceRewardOption() throws Exception {
        // Initialize the database
        randChoiceRewardOptionRepository.saveAndFlush(randChoiceRewardOption);
        int databaseSizeBeforeDelete = randChoiceRewardOptionRepository.findAll().size();

        // Get the randChoiceRewardOption
        restRandChoiceRewardOptionMockMvc.perform(delete("/api/rand-choice-reward-options/{id}", randChoiceRewardOption.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<RandChoiceRewardOption> randChoiceRewardOptions = randChoiceRewardOptionRepository.findAll();
        assertThat(randChoiceRewardOptions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
