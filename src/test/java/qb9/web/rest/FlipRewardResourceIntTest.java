package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.FlipReward;
import qb9.repository.FlipRewardRepository;
import qb9.service.FlipRewardService;
import qb9.web.rest.dto.FlipRewardDTO;
import qb9.web.rest.mapper.FlipRewardMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the FlipRewardResource REST controller.
 *
 * @see FlipRewardResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RewardApp.class)
@WebAppConfiguration
@IntegrationTest
public class FlipRewardResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final BigDecimal DEFAULT_P = new BigDecimal(1);
    private static final BigDecimal UPDATED_P = new BigDecimal(2);

    private static final Integer DEFAULT_QTY = 1;
    private static final Integer UPDATED_QTY = 2;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    @Inject
    private FlipRewardRepository flipRewardRepository;

    @Inject
    private FlipRewardMapper flipRewardMapper;

    @Inject
    private FlipRewardService flipRewardService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restFlipRewardMockMvc;

    private FlipReward flipReward;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FlipRewardResource flipRewardResource = new FlipRewardResource();
        ReflectionTestUtils.setField(flipRewardResource, "flipRewardService", flipRewardService);
        ReflectionTestUtils.setField(flipRewardResource, "flipRewardMapper", flipRewardMapper);
        this.restFlipRewardMockMvc = MockMvcBuilders.standaloneSetup(flipRewardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        flipReward = new FlipReward();
        flipReward.setP(DEFAULT_P);
        flipReward.setQty(DEFAULT_QTY);
        flipReward.setCreatedAt(DEFAULT_CREATED_AT);
        flipReward.setUpdatedAt(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    public void createFlipReward() throws Exception {
        int databaseSizeBeforeCreate = flipRewardRepository.findAll().size();

        // Create the FlipReward
        FlipRewardDTO flipRewardDTO = flipRewardMapper.flipRewardToFlipRewardDTO(flipReward);

        restFlipRewardMockMvc.perform(post("/api/flip-rewards")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(flipRewardDTO)))
                .andExpect(status().isCreated());

        // Validate the FlipReward in the database
        List<FlipReward> flipRewards = flipRewardRepository.findAll();
        assertThat(flipRewards).hasSize(databaseSizeBeforeCreate + 1);
        FlipReward testFlipReward = flipRewards.get(flipRewards.size() - 1);
        assertThat(testFlipReward.getP()).isEqualTo(DEFAULT_P);
        assertThat(testFlipReward.getQty()).isEqualTo(DEFAULT_QTY);
        assertThat(testFlipReward.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testFlipReward.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    public void getAllFlipRewards() throws Exception {
        // Initialize the database
        flipRewardRepository.saveAndFlush(flipReward);

        // Get all the flipRewards
        restFlipRewardMockMvc.perform(get("/api/flip-rewards?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(flipReward.getId().intValue())))
                .andExpect(jsonPath("$.[*].p").value(hasItem(DEFAULT_P.intValue())))
                .andExpect(jsonPath("$.[*].qty").value(hasItem(DEFAULT_QTY)))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)));
    }

    @Test
    @Transactional
    public void getFlipReward() throws Exception {
        // Initialize the database
        flipRewardRepository.saveAndFlush(flipReward);

        // Get the flipReward
        restFlipRewardMockMvc.perform(get("/api/flip-rewards/{id}", flipReward.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(flipReward.getId().intValue()))
            .andExpect(jsonPath("$.p").value(DEFAULT_P.intValue()))
            .andExpect(jsonPath("$.qty").value(DEFAULT_QTY))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR));
    }

    @Test
    @Transactional
    public void getNonExistingFlipReward() throws Exception {
        // Get the flipReward
        restFlipRewardMockMvc.perform(get("/api/flip-rewards/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFlipReward() throws Exception {
        // Initialize the database
        flipRewardRepository.saveAndFlush(flipReward);
        int databaseSizeBeforeUpdate = flipRewardRepository.findAll().size();

        // Update the flipReward
        FlipReward updatedFlipReward = new FlipReward();
        updatedFlipReward.setId(flipReward.getId());
        updatedFlipReward.setP(UPDATED_P);
        updatedFlipReward.setQty(UPDATED_QTY);
        updatedFlipReward.setCreatedAt(UPDATED_CREATED_AT);
        updatedFlipReward.setUpdatedAt(UPDATED_UPDATED_AT);
        FlipRewardDTO flipRewardDTO = flipRewardMapper.flipRewardToFlipRewardDTO(updatedFlipReward);

        restFlipRewardMockMvc.perform(put("/api/flip-rewards")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(flipRewardDTO)))
                .andExpect(status().isOk());

        // Validate the FlipReward in the database
        List<FlipReward> flipRewards = flipRewardRepository.findAll();
        assertThat(flipRewards).hasSize(databaseSizeBeforeUpdate);
        FlipReward testFlipReward = flipRewards.get(flipRewards.size() - 1);
        assertThat(testFlipReward.getP()).isEqualTo(UPDATED_P);
        assertThat(testFlipReward.getQty()).isEqualTo(UPDATED_QTY);
        assertThat(testFlipReward.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testFlipReward.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    public void deleteFlipReward() throws Exception {
        // Initialize the database
        flipRewardRepository.saveAndFlush(flipReward);
        int databaseSizeBeforeDelete = flipRewardRepository.findAll().size();

        // Get the flipReward
        restFlipRewardMockMvc.perform(delete("/api/flip-rewards/{id}", flipReward.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<FlipReward> flipRewards = flipRewardRepository.findAll();
        assertThat(flipRewards).hasSize(databaseSizeBeforeDelete - 1);
    }
}
