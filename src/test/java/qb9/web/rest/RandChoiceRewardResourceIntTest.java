package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.RandChoiceReward;
import qb9.repository.RandChoiceRewardRepository;
import qb9.service.RandChoiceRewardService;
import qb9.service.dto.RandChoiceRewardDTO;
import qb9.service.mapper.RandChoiceRewardMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.RewardStrategyStatus;
/**
 * Test class for the RandChoiceRewardResource REST controller.
 *
 * @see RandChoiceRewardResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RewardApp.class)
public class RandChoiceRewardResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));
    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_CODE = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String DEFAULT_SUMMARY = "AAAAA";
    private static final String UPDATED_SUMMARY = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final RewardStrategyStatus DEFAULT_STATUS = RewardStrategyStatus.DRAFT;
    private static final RewardStrategyStatus UPDATED_STATUS = RewardStrategyStatus.ACTIVE;

    @Inject
    private RandChoiceRewardRepository randChoiceRewardRepository;

    @Inject
    private RandChoiceRewardMapper randChoiceRewardMapper;

    @Inject
    private RandChoiceRewardService randChoiceRewardService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restRandChoiceRewardMockMvc;

    private RandChoiceReward randChoiceReward;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        RandChoiceRewardResource randChoiceRewardResource = new RandChoiceRewardResource();
        ReflectionTestUtils.setField(randChoiceRewardResource, "randChoiceRewardService", randChoiceRewardService);
        this.restRandChoiceRewardMockMvc = MockMvcBuilders.standaloneSetup(randChoiceRewardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RandChoiceReward createEntity(EntityManager em) {
        RandChoiceReward randChoiceReward = new RandChoiceReward();
        randChoiceReward = new RandChoiceReward();
        randChoiceReward.setUid(DEFAULT_UID);
        randChoiceReward.setCode(DEFAULT_CODE);
        randChoiceReward.setSummary(DEFAULT_SUMMARY);
        randChoiceReward.setCreatedAt(DEFAULT_CREATED_AT);
        randChoiceReward.setUpdatedAt(DEFAULT_UPDATED_AT);
        randChoiceReward.setStatus(DEFAULT_STATUS);
        return randChoiceReward;
    }

    @Before
    public void initTest() {
        randChoiceReward = createEntity(em);
    }

    @Test
    @Transactional
    public void createRandChoiceReward() throws Exception {
        int databaseSizeBeforeCreate = randChoiceRewardRepository.findAll().size();

        // Create the RandChoiceReward
        RandChoiceRewardDTO randChoiceRewardDTO = randChoiceRewardMapper.randChoiceRewardToRandChoiceRewardDTO(randChoiceReward);

        restRandChoiceRewardMockMvc.perform(post("/api/rand-choice-rewards")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(randChoiceRewardDTO)))
                .andExpect(status().isCreated());

        // Validate the RandChoiceReward in the database
        List<RandChoiceReward> randChoiceRewards = randChoiceRewardRepository.findAll();
        assertThat(randChoiceRewards).hasSize(databaseSizeBeforeCreate + 1);
        RandChoiceReward testRandChoiceReward = randChoiceRewards.get(randChoiceRewards.size() - 1);
        assertThat(testRandChoiceReward.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testRandChoiceReward.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testRandChoiceReward.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testRandChoiceReward.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testRandChoiceReward.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testRandChoiceReward.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllRandChoiceRewards() throws Exception {
        // Initialize the database
        randChoiceRewardRepository.saveAndFlush(randChoiceReward);

        // Get all the randChoiceRewards
        restRandChoiceRewardMockMvc.perform(get("/api/rand-choice-rewards?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(randChoiceReward.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
                .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getRandChoiceReward() throws Exception {
        // Initialize the database
        randChoiceRewardRepository.saveAndFlush(randChoiceReward);

        // Get the randChoiceReward
        restRandChoiceRewardMockMvc.perform(get("/api/rand-choice-rewards/{id}", randChoiceReward.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(randChoiceReward.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.summary").value(DEFAULT_SUMMARY.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRandChoiceReward() throws Exception {
        // Get the randChoiceReward
        restRandChoiceRewardMockMvc.perform(get("/api/rand-choice-rewards/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRandChoiceReward() throws Exception {
        // Initialize the database
        randChoiceRewardRepository.saveAndFlush(randChoiceReward);
        int databaseSizeBeforeUpdate = randChoiceRewardRepository.findAll().size();

        // Update the randChoiceReward
        RandChoiceReward updatedRandChoiceReward = randChoiceRewardRepository.findOne(randChoiceReward.getId());
        updatedRandChoiceReward.setUid(UPDATED_UID);
        updatedRandChoiceReward.setCode(UPDATED_CODE);
        updatedRandChoiceReward.setSummary(UPDATED_SUMMARY);
        updatedRandChoiceReward.setCreatedAt(UPDATED_CREATED_AT);
        updatedRandChoiceReward.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedRandChoiceReward.setStatus(UPDATED_STATUS);
        RandChoiceRewardDTO randChoiceRewardDTO = randChoiceRewardMapper.randChoiceRewardToRandChoiceRewardDTO(updatedRandChoiceReward);

        restRandChoiceRewardMockMvc.perform(put("/api/rand-choice-rewards")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(randChoiceRewardDTO)))
                .andExpect(status().isOk());

        // Validate the RandChoiceReward in the database
        List<RandChoiceReward> randChoiceRewards = randChoiceRewardRepository.findAll();
        assertThat(randChoiceRewards).hasSize(databaseSizeBeforeUpdate);
        RandChoiceReward testRandChoiceReward = randChoiceRewards.get(randChoiceRewards.size() - 1);
        assertThat(testRandChoiceReward.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testRandChoiceReward.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testRandChoiceReward.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testRandChoiceReward.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testRandChoiceReward.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testRandChoiceReward.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteRandChoiceReward() throws Exception {
        // Initialize the database
        randChoiceRewardRepository.saveAndFlush(randChoiceReward);
        int databaseSizeBeforeDelete = randChoiceRewardRepository.findAll().size();

        // Get the randChoiceReward
        restRandChoiceRewardMockMvc.perform(delete("/api/rand-choice-rewards/{id}", randChoiceReward.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<RandChoiceReward> randChoiceRewards = randChoiceRewardRepository.findAll();
        assertThat(randChoiceRewards).hasSize(databaseSizeBeforeDelete - 1);
    }
}
