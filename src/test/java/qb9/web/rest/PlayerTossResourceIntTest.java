package qb9.web.rest;

import qb9.RewardApp;
import qb9.domain.PlayerToss;
import qb9.repository.PlayerTossRepository;
import qb9.service.PlayerTossService;
import qb9.web.rest.dto.PlayerTossDTO;
import qb9.web.rest.mapper.PlayerTossMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.TossStatus;

/**
 * Test class for the PlayerTossResource REST controller.
 *
 * @see PlayerTossResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RewardApp.class)
@WebAppConfiguration
@IntegrationTest
public class PlayerTossResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));


    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final ZonedDateTime DEFAULT_SENDED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_SENDED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_SENDED_AT_STR = dateTimeFormatter.format(DEFAULT_SENDED_AT);

    private static final ZonedDateTime DEFAULT_PROCESSED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_PROCESSED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_PROCESSED_AT_STR = dateTimeFormatter.format(DEFAULT_PROCESSED_AT);

    private static final TossStatus DEFAULT_STATUS = TossStatus.SENDED;
    private static final TossStatus UPDATED_STATUS = TossStatus.PROCESSED;
    private static final String DEFAULT_RESULT = "AAAAA";
    private static final String UPDATED_RESULT = "BBBBB";

    @Inject
    private PlayerTossRepository playerTossRepository;

    @Inject
    private PlayerTossMapper playerTossMapper;

    @Inject
    private PlayerTossService playerTossService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPlayerTossMockMvc;

    private PlayerToss playerToss;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlayerTossResource playerTossResource = new PlayerTossResource();
        ReflectionTestUtils.setField(playerTossResource, "playerTossService", playerTossService);
        ReflectionTestUtils.setField(playerTossResource, "playerTossMapper", playerTossMapper);
        this.restPlayerTossMockMvc = MockMvcBuilders.standaloneSetup(playerTossResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        playerToss = new PlayerToss();
        playerToss.setCreatedAt(DEFAULT_CREATED_AT);
        playerToss.setUpdatedAt(DEFAULT_UPDATED_AT);
        playerToss.setSendedAt(DEFAULT_SENDED_AT);
        playerToss.setProcessedAt(DEFAULT_PROCESSED_AT);
        playerToss.setStatus(DEFAULT_STATUS);
        playerToss.setResult(DEFAULT_RESULT);
    }

    @Test
    @Transactional
    public void createPlayerToss() throws Exception {
        int databaseSizeBeforeCreate = playerTossRepository.findAll().size();

        // Create the PlayerToss
        PlayerTossDTO playerTossDTO = playerTossMapper.playerTossToPlayerTossDTO(playerToss);

        restPlayerTossMockMvc.perform(post("/api/player-tosses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playerTossDTO)))
                .andExpect(status().isCreated());

        // Validate the PlayerToss in the database
        List<PlayerToss> playerTosses = playerTossRepository.findAll();
        assertThat(playerTosses).hasSize(databaseSizeBeforeCreate + 1);
        PlayerToss testPlayerToss = playerTosses.get(playerTosses.size() - 1);
        assertThat(testPlayerToss.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPlayerToss.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPlayerToss.getSendedAt()).isEqualTo(DEFAULT_SENDED_AT);
        assertThat(testPlayerToss.getProcessedAt()).isEqualTo(DEFAULT_PROCESSED_AT);
        assertThat(testPlayerToss.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPlayerToss.getResult()).isEqualTo(DEFAULT_RESULT);
    }

    @Test
    @Transactional
    public void getAllPlayerTosses() throws Exception {
        // Initialize the database
        playerTossRepository.saveAndFlush(playerToss);

        // Get all the playerTosses
        restPlayerTossMockMvc.perform(get("/api/player-tosses?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(playerToss.getId().intValue())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].sendedAt").value(hasItem(DEFAULT_SENDED_AT_STR)))
                .andExpect(jsonPath("$.[*].processedAt").value(hasItem(DEFAULT_PROCESSED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())));
    }

    @Test
    @Transactional
    public void getPlayerToss() throws Exception {
        // Initialize the database
        playerTossRepository.saveAndFlush(playerToss);

        // Get the playerToss
        restPlayerTossMockMvc.perform(get("/api/player-tosses/{id}", playerToss.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(playerToss.getId().intValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.sendedAt").value(DEFAULT_SENDED_AT_STR))
            .andExpect(jsonPath("$.processedAt").value(DEFAULT_PROCESSED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.result").value(DEFAULT_RESULT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlayerToss() throws Exception {
        // Get the playerToss
        restPlayerTossMockMvc.perform(get("/api/player-tosses/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlayerToss() throws Exception {
        // Initialize the database
        playerTossRepository.saveAndFlush(playerToss);
        int databaseSizeBeforeUpdate = playerTossRepository.findAll().size();

        // Update the playerToss
        PlayerToss updatedPlayerToss = new PlayerToss();
        updatedPlayerToss.setId(playerToss.getId());
        updatedPlayerToss.setCreatedAt(UPDATED_CREATED_AT);
        updatedPlayerToss.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedPlayerToss.setSendedAt(UPDATED_SENDED_AT);
        updatedPlayerToss.setProcessedAt(UPDATED_PROCESSED_AT);
        updatedPlayerToss.setStatus(UPDATED_STATUS);
        updatedPlayerToss.setResult(UPDATED_RESULT);
        PlayerTossDTO playerTossDTO = playerTossMapper.playerTossToPlayerTossDTO(updatedPlayerToss);

        restPlayerTossMockMvc.perform(put("/api/player-tosses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(playerTossDTO)))
                .andExpect(status().isOk());

        // Validate the PlayerToss in the database
        List<PlayerToss> playerTosses = playerTossRepository.findAll();
        assertThat(playerTosses).hasSize(databaseSizeBeforeUpdate);
        PlayerToss testPlayerToss = playerTosses.get(playerTosses.size() - 1);
        assertThat(testPlayerToss.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPlayerToss.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPlayerToss.getSendedAt()).isEqualTo(UPDATED_SENDED_AT);
        assertThat(testPlayerToss.getProcessedAt()).isEqualTo(UPDATED_PROCESSED_AT);
        assertThat(testPlayerToss.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPlayerToss.getResult()).isEqualTo(UPDATED_RESULT);
    }

    @Test
    @Transactional
    public void deletePlayerToss() throws Exception {
        // Initialize the database
        playerTossRepository.saveAndFlush(playerToss);
        int databaseSizeBeforeDelete = playerTossRepository.findAll().size();

        // Get the playerToss
        restPlayerTossMockMvc.perform(delete("/api/player-tosses/{id}", playerToss.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PlayerToss> playerTosses = playerTossRepository.findAll();
        assertThat(playerTosses).hasSize(databaseSizeBeforeDelete - 1);
    }
}
